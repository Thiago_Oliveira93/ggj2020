{
    "id": "eb4a648a-2d93-4b59-9558-d62e540331b1",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font_text_box",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Manaspace",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "3a24bc65-a39e-40b8-afc0-fc93d3d15ce0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 45,
                "offset": 0,
                "shift": 27,
                "w": 27,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "a17a01e1-71dc-4aa4-bf2a-7669abc33a62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 45,
                "offset": 10,
                "shift": 27,
                "w": 10,
                "x": 346,
                "y": 96
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "25e06cc3-bd8d-4ead-b603-b4f5a317d2a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 45,
                "offset": 7,
                "shift": 27,
                "w": 16,
                "x": 328,
                "y": 96
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "f911c7fd-07d3-41bc-8982-9629073fc6d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 306,
                "y": 96
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "84dfd5bd-2f48-415a-8fae-2410b6b29f42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 284,
                "y": 96
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "cc8a4a14-d4d8-46f4-acaa-c61979a84dad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 262,
                "y": 96
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "4c44d76b-2158-4fc6-9e0d-57997290b72a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 240,
                "y": 96
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "477fc48c-63b2-4d7f-a176-4ffd03677898",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 45,
                "offset": 7,
                "shift": 27,
                "w": 6,
                "x": 232,
                "y": 96
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "f8b03225-a183-4ebd-af5c-a2a38470d1df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 45,
                "offset": 10,
                "shift": 27,
                "w": 10,
                "x": 220,
                "y": 96
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "1a263c3b-7a5e-4521-9ff5-08a2fd384646",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 45,
                "offset": 7,
                "shift": 27,
                "w": 10,
                "x": 208,
                "y": 96
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "53a020b1-6993-4068-a589-cf67e5d5b0c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 17,
                "x": 358,
                "y": 96
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "aaaf0184-9c85-486a-a041-ed2e0d49a0d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 186,
                "y": 96
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "1a9b5c9f-e75d-4987-81f4-7a52158203f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 7,
                "x": 155,
                "y": 96
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "b1e89c66-4ed9-4220-8219-51d895c6d219",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 133,
                "y": 96
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "0f94a91d-a523-4dc8-bd8d-f7406facf4d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 7,
                "x": 124,
                "y": 96
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "e02d4245-399e-4b47-8a0d-00314d448e36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 17,
                "x": 105,
                "y": 96
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "44bdd7ae-40a6-4369-9acc-5d249f2a045f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 83,
                "y": 96
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "184a98c7-7177-47f2-bd51-1514287329eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 45,
                "offset": 7,
                "shift": 27,
                "w": 13,
                "x": 68,
                "y": 96
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "1b35e210-3184-4330-906f-63e6786d2abc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 46,
                "y": 96
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "c57231c7-3ceb-42ac-b551-4d311469f921",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 24,
                "y": 96
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "e47266e8-89c1-4578-abb2-930191a0e1b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 2,
                "y": 96
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "819b3944-6d7f-4788-83f8-0f023375b6d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 164,
                "y": 96
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "dd8f35fd-f58d-484e-ae35-011976e3e325",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 377,
                "y": 96
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "43325371-fc3a-46d8-ba7b-09c54b2c6090",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 399,
                "y": 96
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "10f1e3b9-aa20-4293-8749-d4a380e6c691",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 421,
                "y": 96
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "8a1c4326-7031-41cf-a0bf-a01c18a4c4aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 337,
                "y": 143
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "295714e1-7b73-4960-8644-89a60790749a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 45,
                "offset": 10,
                "shift": 27,
                "w": 7,
                "x": 328,
                "y": 143
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "bc20d94c-3886-41ac-989b-01180d6a7d03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 45,
                "offset": 10,
                "shift": 27,
                "w": 7,
                "x": 319,
                "y": 143
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "aa51229e-2af3-4437-a80f-659df1cb6b00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 297,
                "y": 143
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "e23d0d10-77fa-4c8f-a493-b9f89acf402f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 275,
                "y": 143
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "544cc8e5-980c-42b5-9077-db76208e6ead",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 253,
                "y": 143
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "f9f5d37c-9604-45ee-9338-8ecc95743008",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 231,
                "y": 143
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "da8f41c4-3213-4459-9aed-47334195189d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 209,
                "y": 143
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "4ae289eb-0a80-464e-a17a-c4b7c5f18fd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 187,
                "y": 143
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "b3e36c17-745e-405a-8749-d808492d1187",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 165,
                "y": 143
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "fe044e8d-f997-495d-9be8-b1358ff3e576",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 143,
                "y": 143
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "f64295e6-4a5c-4ff7-ab53-00c686ce12a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 121,
                "y": 143
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "fb8c21db-a6f5-47f6-81f7-c3700d24a782",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 99,
                "y": 143
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "a85dd840-7a7a-4309-bac9-f4a6da286cad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 77,
                "y": 143
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "be464358-28b3-4950-8a71-d288f0a11419",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 55,
                "y": 143
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "9e57e09f-7bf0-45b2-88dd-b3f5b5c853f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 33,
                "y": 143
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "30ef612c-2c68-4029-af58-eec4bdf45c72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 45,
                "offset": 10,
                "shift": 27,
                "w": 7,
                "x": 24,
                "y": 143
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "a7e5b400-bc0a-4e3b-84ca-30aa2dbdf643",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 2,
                "y": 143
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "59ab67c4-3833-4ec5-8ef7-8e0739e7552f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 487,
                "y": 96
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "287f9705-cbc6-4e6c-8cc8-b754b6f00b2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 465,
                "y": 96
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "137f8ddf-afaa-421a-b204-70a2dca47e2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 443,
                "y": 96
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "7aaacc84-bf8b-4a34-8acf-e78dae1e79c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 479,
                "y": 49
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "992a93c5-2604-4261-8829-be049d0add48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 457,
                "y": 49
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "06cacc51-df6e-458d-8be6-ce700a465226",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 435,
                "y": 49
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "97df28f9-9edc-46d1-98aa-5fe074546ebc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 469,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "d97d76ed-19e2-48da-b374-fa89208087cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 435,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "ea47ea3b-2a40-48c7-9bdc-b2036143656a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 413,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "d7f84a92-fd27-4a48-93b3-e5d3c1cb1fc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 391,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "1afa7584-4f7a-4524-a8ec-3ed441392ff1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 369,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "ecd0e309-ecc0-4dc7-b949-a582d13dd457",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 347,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "3db7fe76-d147-4bf4-9b5d-dfe951ef163d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 325,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "d0abb2ec-54a1-402d-8d03-693fcea165cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 303,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "f8cb5da6-37f8-4efc-a53e-069084d6bbf0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 281,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "d97b0de8-8fe7-4464-8959-32024e8e9b47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 259,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "d9234a20-6310-48d4-8763-f6927f05eb01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 45,
                "offset": 10,
                "shift": 27,
                "w": 10,
                "x": 457,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "7feb8960-9fec-49a5-b349-b6fdc4c0dfba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 45,
                "offset": 7,
                "shift": 27,
                "w": 16,
                "x": 241,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "76a98330-bdb2-4054-a574-201b8dc637b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 45,
                "offset": 7,
                "shift": 27,
                "w": 10,
                "x": 207,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "b59c0959-66c6-4d61-8c49-4ae671897f97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 185,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "bb20f42b-7ede-4455-9823-fe95b9b94517",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 163,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "d0ad17e7-17b5-49a7-a197-ba1626a39545",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 141,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "76412570-06ba-4103-b97f-13c8b2cd69c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 119,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "bbb13b9e-199e-4f1c-ad26-df148da0112c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 97,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "fd0c42b0-d535-4055-a46f-228add7f1547",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 75,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "f544afcc-9b93-49f6-8535-b9cb82a6e54c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 53,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "8b8c707d-396d-4d02-bb1c-d2975fae5291",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 31,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "f65e4f16-53b1-473b-832d-c12be133a38d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 219,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "44b58aec-e38e-42d5-b8d3-8f29aa31cb7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 2,
                "y": 49
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "7fe7dca7-a568-481d-a914-0907827f763b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 204,
                "y": 49
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "46fa4712-edd8-4327-86bc-e9d1cfb2d314",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 45,
                "offset": 10,
                "shift": 27,
                "w": 7,
                "x": 24,
                "y": 49
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "a677e7a6-088c-4930-8ed8-fc0c2c82e08f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 45,
                "offset": 7,
                "shift": 27,
                "w": 10,
                "x": 401,
                "y": 49
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "9dcf1b42-2222-46cf-8333-6e33cfb294fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 379,
                "y": 49
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "bf5b31aa-598c-49a7-a9c7-2317b2e6c75b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 45,
                "offset": 10,
                "shift": 27,
                "w": 7,
                "x": 370,
                "y": 49
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "4560a8f1-7408-42bc-9316-4af5d0619d85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 348,
                "y": 49
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "3c5e7859-d6ee-4f85-a42b-70bde13b5187",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 326,
                "y": 49
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "94ccff1d-1306-43be-bff4-8928ce73262e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 304,
                "y": 49
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "5cc1d650-622a-4ba0-bad8-1201f1c77cc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 282,
                "y": 49
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "9988bbd9-c94c-46b5-9776-9d8c252446e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 260,
                "y": 49
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "6ca0beae-6a9b-4c57-9a7d-2e450bbd257b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 17,
                "x": 241,
                "y": 49
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "285db456-caaf-4af3-872c-7dd1288348ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 413,
                "y": 49
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "0129dba7-79d8-4eaf-ba28-220f7f747975",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 45,
                "offset": 7,
                "shift": 27,
                "w": 13,
                "x": 226,
                "y": 49
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "8d6d541e-f602-4026-b77a-e307ea5762a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 182,
                "y": 49
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "b59122c0-c7f8-4f02-9d9f-0f6b93295cac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 160,
                "y": 49
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "d7d22ce2-c9ee-4b85-b75b-f6a91936ddce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 138,
                "y": 49
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "e09f9590-3d75-4db3-a06f-297c57628eae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 116,
                "y": 49
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "e64b8ff2-7291-4bc0-9ba0-7b3269a47952",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 94,
                "y": 49
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "42d852b3-8ab7-403b-97a4-b9e84c411ea0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 72,
                "y": 49
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "ef67ac38-fd26-43f3-a667-4b4dd326f69e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 45,
                "offset": 7,
                "shift": 27,
                "w": 13,
                "x": 57,
                "y": 49
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "a394d147-afb8-4b6f-933b-977c129d64f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 45,
                "offset": 10,
                "shift": 27,
                "w": 7,
                "x": 48,
                "y": 49
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "956362b8-f819-45c4-bed7-c78ef24c23a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 45,
                "offset": 7,
                "shift": 27,
                "w": 13,
                "x": 33,
                "y": 49
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "0b7b0a18-8091-4a48-8dcd-901a217e5af0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 45,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 359,
                "y": 143
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "f6437119-b45f-430e-862d-f72cf91fccdd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 45,
                "offset": 8,
                "shift": 39,
                "w": 23,
                "x": 381,
                "y": 143
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 30,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}