{
    "id": "eb4a648a-2d93-4b59-9558-d62e540331b1",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font_text_box",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Manaspace",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "7f81676c-01bf-4394-b88b-63d01079ba1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 33,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "6fb723ea-3c1f-44e0-bb08-82ba8c74bfcb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 33,
                "offset": 7,
                "shift": 19,
                "w": 8,
                "x": 50,
                "y": 142
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "1ffd5577-eabf-4d1f-8ddd-f0f0fdbb835b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 33,
                "offset": 5,
                "shift": 19,
                "w": 12,
                "x": 36,
                "y": 142
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "fce9702f-6f89-435a-a1df-37c94267f511",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 19,
                "y": 142
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "f038f1d1-31a2-4ae2-b108-347ac0f5f442",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 2,
                "y": 142
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "18ef7a9c-779d-4862-b43a-caafedcf1a0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 222,
                "y": 107
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "0f904364-b3b4-483a-b30d-56f6dcc56130",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 205,
                "y": 107
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "58a465bb-e4e1-4ebd-97d2-d52910a11f63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 33,
                "offset": 5,
                "shift": 19,
                "w": 5,
                "x": 198,
                "y": 107
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "623f298e-e244-4734-8130-0f11b22d9d02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 33,
                "offset": 7,
                "shift": 19,
                "w": 8,
                "x": 188,
                "y": 107
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "074ed58f-05ef-4268-9369-52d11e76157c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 33,
                "offset": 5,
                "shift": 19,
                "w": 7,
                "x": 179,
                "y": 107
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "6bb5e514-6c5a-45ce-96e5-921432775356",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 13,
                "x": 60,
                "y": 142
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "fa0fe6f9-df50-4fa8-85c9-38f845cbfa2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 162,
                "y": 107
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "bc7befec-559b-49bd-8ec6-7ee57597ca22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 5,
                "x": 138,
                "y": 107
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "55dbc47d-e3c8-4ae4-b8b6-58fa8c889f1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 121,
                "y": 107
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "fa66ed92-840b-40fd-8665-910084df326f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 5,
                "x": 114,
                "y": 107
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "70a0ac27-8509-47f1-b1ba-552801c02fe1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 13,
                "x": 99,
                "y": 107
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "57d0973e-4a17-4b1e-b0c1-3487a4452058",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 82,
                "y": 107
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "22968b3c-ab04-4c8e-a00e-3019b496269d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 33,
                "offset": 5,
                "shift": 19,
                "w": 10,
                "x": 70,
                "y": 107
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "55b39da9-7066-41c9-a32a-99b12407b7fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 53,
                "y": 107
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "f2600d55-86c7-4ec0-b8a5-654c9a57d2d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 36,
                "y": 107
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "eb27b3cf-f405-4ff1-b905-f1e979530321",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 19,
                "y": 107
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "5c940219-53f9-44a7-a0fd-76824255c21d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 145,
                "y": 107
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "afbf4318-bd63-46e7-ac5a-dfd6502e7d94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 75,
                "y": 142
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "bc6ef614-3d18-4a62-9e37-4e3fe658c74d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 92,
                "y": 142
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "21d02316-e8f7-45dd-a7a3-0cf02891001d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 109,
                "y": 142
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "96efadc5-3c6e-4fec-b853-7f0975ec1ba6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 186,
                "y": 177
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "86db1c48-266f-49e1-9d35-e51c2bc4d8ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 33,
                "offset": 7,
                "shift": 19,
                "w": 5,
                "x": 179,
                "y": 177
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "f74d3e4a-63a8-4209-a572-9ecfa14414cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 33,
                "offset": 7,
                "shift": 19,
                "w": 5,
                "x": 172,
                "y": 177
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "481e8538-8a16-409e-b260-acfb80eca627",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 155,
                "y": 177
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "57a75072-552a-4b1d-81a0-382737fa652d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 138,
                "y": 177
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "989760bf-bb66-4031-802a-fd5d8a3ee802",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 121,
                "y": 177
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "f0a56007-544e-4b68-8577-325793104e28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 104,
                "y": 177
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "fb32866d-6b1d-45b3-9d21-637122739811",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 87,
                "y": 177
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "b3fcc020-7d61-4d0f-95d3-558cb82c15e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 70,
                "y": 177
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "869c5095-3047-4a4c-92c8-bce6880b16c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 53,
                "y": 177
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "a21cd21f-451a-4a18-976c-38824186192c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 36,
                "y": 177
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "023f24af-4f46-42d5-8c95-a0d261598cd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 19,
                "y": 177
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "a533f8fb-d836-4bc4-b667-90c0baed4cf7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 2,
                "y": 177
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "9e299161-3e1c-42cf-8321-8b698dcb9984",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 235,
                "y": 142
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "a5663287-984e-4ba2-8a69-41add528b6f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 218,
                "y": 142
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "3003d188-50e1-4722-a964-b46baa3c5ea8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 201,
                "y": 142
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "8f75bc91-83ed-40fc-a084-16280f4fb2d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 33,
                "offset": 7,
                "shift": 19,
                "w": 5,
                "x": 194,
                "y": 142
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "186d4d64-7327-4cd2-b7d6-7751f36ab79d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 177,
                "y": 142
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "cfb29de7-52b7-4a5d-b592-03271af54237",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 160,
                "y": 142
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "632689db-8d17-49f5-95fb-13a4c0d73d5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 143,
                "y": 142
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "318114dd-debb-4983-ad38-afad81f0fd8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 126,
                "y": 142
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "1ab0489b-91e3-4a45-8b1a-a129ca5bc91b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 2,
                "y": 107
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "1808fe04-bb0c-47af-ae2f-d0619cd102aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 232,
                "y": 72
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "44ac3aab-f5fa-4b9c-aa2e-96a9a637db28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 215,
                "y": 72
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "e1423751-81cd-4d4e-b643-23c2c83e1ff0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 114,
                "y": 37
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "6233b735-40ad-49f6-b324-77f9214dcdf3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 87,
                "y": 37
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "182c3c4b-68cc-4534-b876-f0164b1dc7b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 70,
                "y": 37
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "7d45a3c1-50da-4ce7-bf60-26a3174edc2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 53,
                "y": 37
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "31c2f009-a986-49d9-b7b1-4b1773fdd9dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 36,
                "y": 37
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "9b055e93-8955-4c19-af36-dfc355fe2337",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 19,
                "y": 37
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "564288e7-100b-4ce9-9162-4b8a38749391",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 2,
                "y": 37
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "226aa9c3-da9d-41de-a3c2-336fea4f207b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 233,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "eab7d6a7-4933-4b9a-ab66-fa6fe36f93e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 216,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "5ed31b06-4c9f-4f73-a030-aac60df942ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 199,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "79143dae-c427-4e17-ad7f-6d7455cbd0af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 33,
                "offset": 7,
                "shift": 19,
                "w": 8,
                "x": 104,
                "y": 37
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "c927f336-c71e-4d54-800e-a8f9afe90d32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 33,
                "offset": 5,
                "shift": 19,
                "w": 12,
                "x": 185,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "f3b70c9a-9190-42fd-89d1-da6a6c67105c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 33,
                "offset": 5,
                "shift": 19,
                "w": 7,
                "x": 159,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "b9e601be-3b92-4047-beec-6d74cb6cb8b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 142,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "6bc4b39f-ad24-4954-a1d8-542df2fd5344",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 125,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "df654ca7-ce06-407e-b41a-5b375414912f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 108,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "357ff7c0-6cdf-434e-b5e1-7a6c5a289b61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 91,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "54226ef9-bf27-4232-bece-d8b007f685c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 74,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "d1fb6eeb-13a1-4f57-85ce-350a62b72d4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 57,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "0bf6e2db-4e56-48a8-8987-6337fc84f923",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 40,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "88e1492b-9334-48a4-bb25-d632a30b9dc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 23,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "ac338468-7793-4a4c-b78c-110568f21c54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 168,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "aea1dcc2-9a9d-4f63-95d3-bfb1b7f84b26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 131,
                "y": 37
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "c4859672-8f0c-4038-8100-2630cdd99cfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 36,
                "y": 72
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "9000c8a8-8e88-47a5-a1d8-09b0ab0fcc1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 33,
                "offset": 7,
                "shift": 19,
                "w": 5,
                "x": 148,
                "y": 37
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "e683b467-f951-4416-af52-6835b4b09392",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 33,
                "offset": 5,
                "shift": 19,
                "w": 7,
                "x": 189,
                "y": 72
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "386fd604-1bbf-4e0a-a234-8546f0fd218a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 172,
                "y": 72
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "e30fba69-a595-4805-82ea-90edb6bb215e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 33,
                "offset": 7,
                "shift": 19,
                "w": 5,
                "x": 165,
                "y": 72
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "cc0f7626-a39c-4833-b129-ea1290042a89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 148,
                "y": 72
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "3ce67ad9-1833-41ee-8224-e0193ac3bd71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 131,
                "y": 72
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "42d941b2-67fe-4f0c-908c-313ff21b2263",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 114,
                "y": 72
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "a6676c7a-6611-4efb-abbe-615135d18f29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 97,
                "y": 72
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "230eece6-943e-413b-b09b-8632e723d167",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 80,
                "y": 72
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "c7dffde3-3fa3-4da2-b41c-5d28ab2d7699",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 13,
                "x": 65,
                "y": 72
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "31a2f64a-de2f-40d9-b0ee-d8c500f19054",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 198,
                "y": 72
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "8e4fe3b2-24d1-4d38-9c52-be14a377ea15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 33,
                "offset": 5,
                "shift": 19,
                "w": 10,
                "x": 53,
                "y": 72
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "a89bb9c4-182b-4e04-95a2-53e86b25960a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 19,
                "y": 72
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "61b3ff81-99d2-458c-abf7-e31b452eea77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 2,
                "y": 72
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "050625e4-6045-46e7-a74b-84b2566a4258",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 237,
                "y": 37
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "85c6c21b-4fa7-4ccd-96e6-13b5ebcd112e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 220,
                "y": 37
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "78df5615-a6a4-4b91-95a5-febf4ae470cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 203,
                "y": 37
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "7a98f314-c71a-4f3d-b8b5-fc4b163d37be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 186,
                "y": 37
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "184d3d39-afd4-48ea-bc03-74b3748d2837",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 33,
                "offset": 5,
                "shift": 19,
                "w": 10,
                "x": 174,
                "y": 37
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "e21b9800-e4c9-4907-8948-b1cf1d223f42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 33,
                "offset": 7,
                "shift": 19,
                "w": 5,
                "x": 167,
                "y": 37
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "6eed0e77-73a6-4b19-98dd-4507b3a77b59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 33,
                "offset": 5,
                "shift": 19,
                "w": 10,
                "x": 155,
                "y": 37
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "13e20420-86f0-4b56-a896-f5c5d4e07b67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 203,
                "y": 177
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "4d3e8a6f-9180-4650-8ec4-c63f83d04d43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 33,
                "offset": 6,
                "shift": 28,
                "w": 16,
                "x": 220,
                "y": 177
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 22,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}