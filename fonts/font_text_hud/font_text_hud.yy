{
    "id": "9e341eb0-a424-4929-af43-45c18c0c987f",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font_text_hud",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Manaspace",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "a34c2db7-580b-427f-9fcd-21af77aeed33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "2e75700c-bc5d-453b-ade6-386c95e1c724",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 18,
                "offset": 4,
                "shift": 11,
                "w": 4,
                "x": 115,
                "y": 42
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "a6def5e2-6e7b-40d2-a351-25588ed9e704",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 18,
                "offset": 3,
                "shift": 11,
                "w": 6,
                "x": 107,
                "y": 42
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "7e994735-bc53-4a0d-b698-3dcd9314fd20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 97,
                "y": 42
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "cc7b3e05-b43e-4115-b632-49b844ffd1f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 87,
                "y": 42
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "763f8e36-4800-4f19-b6c0-b3866c9d48de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 77,
                "y": 42
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "982bcea2-4397-4e73-adfa-09a4efc0d7c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 67,
                "y": 42
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "c6a449e3-22f3-4e9e-b048-6d34c073932b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 18,
                "offset": 3,
                "shift": 11,
                "w": 2,
                "x": 63,
                "y": 42
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "5cbcad1a-bf95-4690-bbef-1e9200d89fb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 18,
                "offset": 4,
                "shift": 11,
                "w": 4,
                "x": 57,
                "y": 42
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "61b87524-10d5-4f1b-b037-7e1eee32b2a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 18,
                "offset": 3,
                "shift": 11,
                "w": 4,
                "x": 51,
                "y": 42
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "ed26bc17-5070-4c7e-b2be-42f6106a1d76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 7,
                "x": 121,
                "y": 42
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "07b0eebf-900a-412f-8185-05d7d5fcb176",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 41,
                "y": 42
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "947971d5-2c5e-4bb6-84e7-551143b9ebc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 3,
                "x": 26,
                "y": 42
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "75b5a764-90d3-4720-b634-66b884b5353d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 16,
                "y": 42
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "e5a3190f-406a-46e3-a5bf-7cf662f494d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 3,
                "x": 11,
                "y": 42
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "99c87b5e-dc51-4e13-9e0c-50bd67dfa5a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 7,
                "x": 2,
                "y": 42
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "e6af7e29-58f9-4500-a7b5-db39163ea092",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 243,
                "y": 22
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "3d10e62d-37cb-4b54-b1e6-de803a0cd8f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 18,
                "offset": 3,
                "shift": 11,
                "w": 5,
                "x": 236,
                "y": 22
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "e541f2e1-c196-4fdf-8024-97397d656862",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 226,
                "y": 22
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "87a064df-0c2b-4927-a507-2906afe722c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 216,
                "y": 22
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "da275bd8-3fff-4f9f-a205-d406e203041b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 206,
                "y": 22
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "e7de2391-791f-4f6c-ba23-bdb5a591e585",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 31,
                "y": 42
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "e8f2ea71-0886-46e0-8311-d8b32dd983b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 130,
                "y": 42
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "36add064-6139-4825-bcc1-f80c94fcad35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 140,
                "y": 42
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "bb424438-cc9c-4d1a-b470-813f7e28c97f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 150,
                "y": 42
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "798cfd71-14ac-4567-97aa-c52c6199492d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 92,
                "y": 62
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "a6ae7c92-9528-4e60-9560-58b5c632f08a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 18,
                "offset": 4,
                "shift": 11,
                "w": 3,
                "x": 87,
                "y": 62
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "e3a6f987-8e50-44c1-b995-c7854087c7ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 18,
                "offset": 4,
                "shift": 11,
                "w": 3,
                "x": 82,
                "y": 62
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "282424ea-1543-490f-821e-04627b64ebd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 72,
                "y": 62
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "508d4590-af87-45cc-a29a-a344b16f3521",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 62,
                "y": 62
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "16deda36-e2ee-4e86-afe3-4d07d50aa23d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 52,
                "y": 62
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "d3585d91-e323-47aa-8041-120fb34181e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 42,
                "y": 62
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "b0af549a-4890-444b-9df1-d9abe0c2ca53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 32,
                "y": 62
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "3e196e56-a8a4-400a-baa1-2895dae0083f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 22,
                "y": 62
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "dc803bb6-c9fd-47e3-bfd2-e1fd71ae61c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 12,
                "y": 62
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "0eaa2278-a3e0-4b09-801d-f685375b22cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "f4e1f911-de1c-4c9c-9f30-96a5d709c961",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 245,
                "y": 42
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "e93b02e3-3574-4c0f-9655-9471ad304e81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 235,
                "y": 42
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "0fbe2553-ca33-4ff1-84da-b8fa59d5a3ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 225,
                "y": 42
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "8bc71b2e-ac08-444b-90c6-8690b474bf9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 215,
                "y": 42
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "6c097bb6-8f20-4c05-b847-f0e13ffa5a36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 205,
                "y": 42
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "60d681e8-9129-4bda-ada7-dbe3c991e390",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 18,
                "offset": 4,
                "shift": 11,
                "w": 3,
                "x": 200,
                "y": 42
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "12d41f0b-4099-4465-9eb9-8932101562ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 190,
                "y": 42
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "2d567678-b8b3-4964-a3df-2d880fd8e859",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 180,
                "y": 42
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "7e82c9c0-874f-4a64-8515-505f8a06f9bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 170,
                "y": 42
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "8d7c76f5-e7fe-452e-8bb2-140e559c5d73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 160,
                "y": 42
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "7c79d065-be7e-4a32-8d3e-c91d411dec4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 196,
                "y": 22
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "2a388771-e3e1-45f1-a0ec-758fbb9ce63d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 186,
                "y": 22
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "44fc9905-455d-4b15-b2d1-900103557e4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 176,
                "y": 22
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "132b786b-e098-4a92-8b7b-b496d1ad4491",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 215,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "d43998f0-3168-4b19-901e-e721db4e811f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 199,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "0f8c444d-f58b-4c60-b3ae-4e18bfa62533",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 189,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "f6147732-e419-45c6-936a-560de0d2eeb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 179,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "d3929edd-7061-414f-89d8-5b3cdd4d4544",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 169,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "0c589a8a-c738-407f-a571-e9b7ee74d9bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 159,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "5cd7824b-269d-430f-b7ea-69a859eea084",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 149,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "70421cb5-4e5a-4c24-9e64-1df395f39f4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 139,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "68e9a239-8748-44dc-8204-16463c54d8a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 129,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "ba0d3793-14de-4b3f-b8fe-13528860cc50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 119,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "88207c6e-714c-43bb-a4ef-7f80477e94a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 18,
                "offset": 4,
                "shift": 11,
                "w": 4,
                "x": 209,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "c9188321-8733-4ee9-8939-e1cb5095d968",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 18,
                "offset": 3,
                "shift": 11,
                "w": 6,
                "x": 111,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "772176c9-9697-4f3d-a3d3-24ae0cfd3ec2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 18,
                "offset": 3,
                "shift": 11,
                "w": 4,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "a7675ce4-e9a9-40ec-afa7-e2dfbc0d590f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 85,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "8dd40e1a-87c6-4d82-b2dc-5dd4039147b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 75,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "89aade52-4364-4b96-8ecc-7a93c1da3e68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 65,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "3195473d-f543-4d1f-8ac7-c180a24b4918",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 55,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "01964409-6b5a-4320-8791-b5ac04652bbe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 45,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "4b91b87f-187a-4552-adc5-10513cf8b88b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 35,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "0fffae39-ceb0-4c17-9517-f7b3cac690ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 25,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "e14ed3a5-ba3e-442c-a26d-431fe3546439",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 15,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "58d019dc-07b0-4a67-b56e-dc16e8d29752",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 101,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "9949db51-bad3-4cdc-8877-53ac07386e2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 225,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "13b79485-3865-4e33-8ab9-fd25096035e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 69,
                "y": 22
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "dd9426eb-7807-4509-a722-711f7874b58a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 18,
                "offset": 4,
                "shift": 11,
                "w": 3,
                "x": 235,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "f3c75282-9e9d-4197-b7de-a75f3e1739a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 18,
                "offset": 3,
                "shift": 11,
                "w": 4,
                "x": 160,
                "y": 22
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "cb0c0ea9-e09c-4666-9b22-aa84ae767293",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 150,
                "y": 22
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "9b822874-ea8a-4697-8376-cfb526d71221",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 18,
                "offset": 4,
                "shift": 11,
                "w": 3,
                "x": 145,
                "y": 22
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "197f3482-2dcc-4fa6-a880-5768328d9267",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 135,
                "y": 22
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "faf60e7c-b778-4011-a340-bfa06708be96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 125,
                "y": 22
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "837501e4-3d46-4996-9893-6baa3ce3fd93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 115,
                "y": 22
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "e5ff96a5-9091-4dc7-a1bf-4bba9042f3b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 105,
                "y": 22
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "810de2eb-b13d-468f-9f55-ddb77691b413",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 95,
                "y": 22
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "557749ac-6c10-4ddd-96cf-6f6aaa960ef2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 7,
                "x": 86,
                "y": 22
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "208c2fc5-b5ac-40cc-a2d3-d4c35e58cef0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 166,
                "y": 22
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "64a70e67-e3fe-4bea-be98-5bb99504f3df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 18,
                "offset": 3,
                "shift": 11,
                "w": 5,
                "x": 79,
                "y": 22
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "5b054589-00ee-45c4-b3ad-1c3665515e8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 59,
                "y": 22
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "e08f0be3-12e5-4ced-b3be-5def78e03239",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 49,
                "y": 22
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "7f703795-c31f-4da3-8871-b9b62164f0ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 39,
                "y": 22
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "788d26c3-dbb2-4aae-a99a-2ed7e20c6df3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 29,
                "y": 22
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "d497e19c-70b0-4234-8b7c-5c5cc387490b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 19,
                "y": 22
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "dc00568a-d7cd-4fd0-b2bc-fca30c8f57a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 9,
                "y": 22
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "cfb212e2-a4ac-43d5-bdd0-364cc38c32a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 18,
                "offset": 3,
                "shift": 11,
                "w": 5,
                "x": 2,
                "y": 22
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "ddca75f4-114e-4cd1-9bfc-97f22608b58c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 18,
                "offset": 4,
                "shift": 11,
                "w": 3,
                "x": 247,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "f356d383-1660-4295-b6d2-47bf0b5dfdeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 18,
                "offset": 3,
                "shift": 11,
                "w": 5,
                "x": 240,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "502bd5fe-d0ed-434b-b60c-f17917a1225e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 102,
                "y": 62
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "42bf8bfe-cfed-4f08-a612-cc27ae9cda1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 18,
                "offset": 3,
                "shift": 16,
                "w": 9,
                "x": 112,
                "y": 62
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}