{
    "id": "9e341eb0-a424-4929-af43-45c18c0c987f",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font_text_hud",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Manaspace",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "cd6cc887-835f-4aa0-9f0c-9817bccb6538",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 33,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "632e45b1-821d-434c-956e-67ebb6093c12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 33,
                "offset": 7,
                "shift": 19,
                "w": 8,
                "x": 50,
                "y": 142
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "914abdf1-da5d-469e-8668-70a1dd900b37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 33,
                "offset": 5,
                "shift": 19,
                "w": 12,
                "x": 36,
                "y": 142
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "f893ffc9-626d-48d6-aacb-059e3b88eba2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 19,
                "y": 142
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "b7636c3e-140c-4c86-956c-eb859bb64bb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 2,
                "y": 142
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "cdc49e1d-c17e-4ba6-a533-45631868d59c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 222,
                "y": 107
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "c4d24e73-2279-4f69-b32f-130ed58e582b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 205,
                "y": 107
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "97c3b427-5d80-4b81-acda-8d39abb2bd2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 33,
                "offset": 5,
                "shift": 19,
                "w": 5,
                "x": 198,
                "y": 107
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "559f059f-a552-4993-963f-9f3dace1cf56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 33,
                "offset": 7,
                "shift": 19,
                "w": 8,
                "x": 188,
                "y": 107
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "9f622896-5c15-4a12-abf2-30149c9e15f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 33,
                "offset": 5,
                "shift": 19,
                "w": 7,
                "x": 179,
                "y": 107
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "72a5d5c0-8d36-427a-b4bd-cce8cfd444a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 13,
                "x": 60,
                "y": 142
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "499cb414-e958-4c57-973e-67e7b03a97ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 162,
                "y": 107
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "896a2a94-f84c-43c6-a4b5-c17a6ef8843a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 5,
                "x": 138,
                "y": 107
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "acfcbd46-1035-4d75-9fa7-83739b7d03d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 121,
                "y": 107
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "3ace1031-fefa-4afc-bc29-29c0042ab448",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 5,
                "x": 114,
                "y": 107
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "119764b0-0fde-4882-a62d-78a135f3868a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 13,
                "x": 99,
                "y": 107
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "1e3b2213-f426-47fc-aa00-8fc57374c095",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 82,
                "y": 107
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "402a0bc7-1640-4715-9587-7ebbabf05b5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 33,
                "offset": 5,
                "shift": 19,
                "w": 10,
                "x": 70,
                "y": 107
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "05d473db-9d4d-435a-8141-17850c01f7d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 53,
                "y": 107
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "3566867d-cc1d-4ebe-8f9c-7847eb306e8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 36,
                "y": 107
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "5c1c2974-f894-43df-b105-9ddde749d25a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 19,
                "y": 107
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "46dc9fd0-5983-44a5-8b7e-2ea449a48214",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 145,
                "y": 107
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "cc4c59b0-ab0d-46b5-86bc-ae53341667d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 75,
                "y": 142
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "c0055877-b0d3-422d-9aa1-adcff46071b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 92,
                "y": 142
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "761f3bde-0b9c-48c3-8e60-d45d5c2e4b01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 109,
                "y": 142
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "b2e4ac57-e467-453d-a4fa-3b397fb9a4bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 186,
                "y": 177
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "440a91db-69a5-4bf1-8a80-c5a2809a359c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 33,
                "offset": 7,
                "shift": 19,
                "w": 5,
                "x": 179,
                "y": 177
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "ff302f4b-46ac-449b-977a-209a0a7d7d7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 33,
                "offset": 7,
                "shift": 19,
                "w": 5,
                "x": 172,
                "y": 177
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "6a6dace5-dc10-4bb9-ab85-f987e66471d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 155,
                "y": 177
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "053f05f0-a987-44ae-b308-0b4a4b85b694",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 138,
                "y": 177
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "6b908bde-ccd6-42a4-9f78-5e278f424166",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 121,
                "y": 177
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "83f0ba21-a9c8-4d41-aa56-1c49bf97645e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 104,
                "y": 177
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "486a4278-4634-46a6-bc6f-effc5df5fffb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 87,
                "y": 177
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "7d573399-6150-4ccb-b59f-3ecb9639af33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 70,
                "y": 177
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "9b846ba3-eda5-4ece-b846-cfc6ae243750",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 53,
                "y": 177
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "fddbd481-4ad2-44ee-840e-e09167c48a73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 36,
                "y": 177
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "2724b6fa-8aa7-4dd2-9476-33b4822304e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 19,
                "y": 177
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "28dcf03b-606c-4e87-808e-59bca4da7e2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 2,
                "y": 177
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "4782a042-23df-44fb-9c7e-82cd79b87234",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 235,
                "y": 142
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "38c83140-b9cc-417a-b67f-8c465dbc47ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 218,
                "y": 142
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "f83d974c-42bb-464e-a539-f149b968fd94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 201,
                "y": 142
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "05519f0e-f1f5-4469-83c4-134a4fcfdf91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 33,
                "offset": 7,
                "shift": 19,
                "w": 5,
                "x": 194,
                "y": 142
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "ba9787ef-9461-4e7e-b500-114a24c2a033",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 177,
                "y": 142
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "8d9f2133-415c-4f5f-80fc-be067273225d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 160,
                "y": 142
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "464ee255-3b7e-4651-b68e-0afbeca3bca6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 143,
                "y": 142
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "14064767-160f-4c69-81d7-2663a0b29a6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 126,
                "y": 142
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "f340eafd-ef0d-47e0-bc7a-65b22bf5291f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 2,
                "y": 107
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "522a3216-d020-4b0d-abc3-6e43589f7fd0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 232,
                "y": 72
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "325cdab4-d99b-487a-b520-3ddd648250ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 215,
                "y": 72
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "33dc04ea-3985-42d9-8dc9-2895afa7b0cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 114,
                "y": 37
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "4cf1970f-a11c-4504-8665-49e00b4af96e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 87,
                "y": 37
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "a56a9c33-4cbe-4ef9-9673-0e714e38972f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 70,
                "y": 37
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "464fc669-f5cf-4857-8a5a-3eecef62597a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 53,
                "y": 37
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "fc4ddfaa-400b-4d59-bd08-53aea81a720a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 36,
                "y": 37
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "39040993-dd9b-42d8-acd5-42c7c98253f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 19,
                "y": 37
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "bfb715ee-764e-4503-9ac3-15fdcb6f8f9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 2,
                "y": 37
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "6cb1ad68-9137-43ae-8ddd-5d13ea7bf9a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 233,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "def0dfb1-2989-4166-9932-cff9ba62995d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 216,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "5258fc96-8929-49b6-8dd5-14f242ac3da1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 199,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "c2868f2d-0293-4ac4-87cd-9d5230d0a050",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 33,
                "offset": 7,
                "shift": 19,
                "w": 8,
                "x": 104,
                "y": 37
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "8a6915ca-c234-49df-8d55-1a2e40503cad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 33,
                "offset": 5,
                "shift": 19,
                "w": 12,
                "x": 185,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "125fc88a-f454-4df1-9be2-92be8dd80a45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 33,
                "offset": 5,
                "shift": 19,
                "w": 7,
                "x": 159,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "63499a59-bb07-49b9-aed9-32eeb1a145a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 142,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "2cb3e1c2-c83b-4d36-80c5-d3a445a50b32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 125,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "4909d341-a1c9-47a7-9a5a-03aea2a6fcd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 108,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "e9a0029c-3610-40c0-89b5-f19615a61f33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 91,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "e9546c9f-5f5e-4497-a91d-aa53a9586e6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 74,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "ada4f952-7b84-46a7-9a1c-f03349f0466d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 57,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "fb674b54-96c0-4916-80b0-dc98194a9a36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 40,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "3c4e6e62-8c4d-4a85-b3c6-a7d3d6d8010e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 23,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "c2275f07-8894-4d1f-8c3f-c92e8e5fffc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 168,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "18204474-bd72-4409-89b8-dd0e64044134",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 131,
                "y": 37
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "bbe1c31c-e70c-4384-9aaa-9ee308c593df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 36,
                "y": 72
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "cb4ea4ba-dfb4-4c27-b856-4a50f7b7de2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 33,
                "offset": 7,
                "shift": 19,
                "w": 5,
                "x": 148,
                "y": 37
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "bfe858c6-fc0e-478b-9425-d16e9804235e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 33,
                "offset": 5,
                "shift": 19,
                "w": 7,
                "x": 189,
                "y": 72
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "9684f890-b283-402c-a7d6-3a3e13ab83a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 172,
                "y": 72
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "b1a99ffa-783c-4206-a8df-115ddddc5575",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 33,
                "offset": 7,
                "shift": 19,
                "w": 5,
                "x": 165,
                "y": 72
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "f22ba518-d6b9-43a2-a281-0209d7fdc762",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 148,
                "y": 72
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "8f268d40-050b-4585-aace-352e1248c232",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 131,
                "y": 72
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "e03fdaed-4c95-43dd-84d3-bbc3434bc366",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 114,
                "y": 72
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "70850f41-e9a3-4405-b852-86f6b5d3552b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 97,
                "y": 72
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "9c54edce-80d9-4117-9d74-07320f9f6868",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 80,
                "y": 72
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "e887f5e2-dba4-45cf-b5c3-64ca645af986",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 13,
                "x": 65,
                "y": 72
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "64a9f092-ddf8-4854-8422-222ce8243916",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 198,
                "y": 72
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "51a0c6bf-fca7-4c48-b560-d615414cc608",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 33,
                "offset": 5,
                "shift": 19,
                "w": 10,
                "x": 53,
                "y": 72
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "4dac3a5b-791f-47cf-9424-b3d3dec613a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 19,
                "y": 72
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "d38db677-bf68-424c-9e26-5f6ef1f3171f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 2,
                "y": 72
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "67fcaa36-899a-4788-95c0-8a780d39be5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 237,
                "y": 37
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "c70e2ac9-0ef2-470f-8f7f-6625c48799c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 220,
                "y": 37
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "f8f42603-2662-4c25-a1b4-4a9f1f9748f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 203,
                "y": 37
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "b584c4a1-242d-4a77-b2ec-7c0f8c67eef3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 186,
                "y": 37
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "3ae55ea4-db2f-4462-9188-d77f4769d151",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 33,
                "offset": 5,
                "shift": 19,
                "w": 10,
                "x": 174,
                "y": 37
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "18b8b210-2eaf-4263-a9ad-1eb84effc264",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 33,
                "offset": 7,
                "shift": 19,
                "w": 5,
                "x": 167,
                "y": 37
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "785c33c7-77e7-4dc2-95a0-b2c86e6894b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 33,
                "offset": 5,
                "shift": 19,
                "w": 10,
                "x": 155,
                "y": 37
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "567e78ce-1ab4-484d-8f4c-8ea0e9db9005",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 33,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 203,
                "y": 177
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "f3a50382-9e64-4a44-b63b-a4ce8ec365d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 33,
                "offset": 6,
                "shift": 28,
                "w": 16,
                "x": 220,
                "y": 177
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 22,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}