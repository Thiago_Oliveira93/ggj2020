{
    "id": "482ae936-eb42-4e02-b3e1-2abda13a7f06",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font_menu",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Manaspace",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "4b6de26f-b438-45f4-80f9-819dc61eccce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 24,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "5a1538c8-408b-4a30-841b-2100e72a06ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 24,
                "offset": 5,
                "shift": 14,
                "w": 6,
                "x": 120,
                "y": 80
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "4cd439fb-90f9-4be1-b2c6-a5457742fb8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 24,
                "offset": 3,
                "shift": 14,
                "w": 10,
                "x": 108,
                "y": 80
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "4b593be8-abe3-4d10-9f25-819271afd296",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 94,
                "y": 80
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "213abe18-1c7c-4a13-a6dc-4d934d18d139",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 80,
                "y": 80
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "7b4116b9-d161-4b95-971b-707e218508be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 66,
                "y": 80
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "3c4c4ffe-b0bb-49bc-ad9c-499758b05a99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 52,
                "y": 80
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "6b37b8b5-a6f7-4f0c-afef-57407708efbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 24,
                "offset": 3,
                "shift": 14,
                "w": 4,
                "x": 46,
                "y": 80
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "180a3ccb-c51f-467e-958d-8107d235060a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 24,
                "offset": 5,
                "shift": 14,
                "w": 6,
                "x": 38,
                "y": 80
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "9cab174b-a3aa-4e76-bcd0-c982cf1379b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 24,
                "offset": 3,
                "shift": 14,
                "w": 6,
                "x": 30,
                "y": 80
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "0cb7614c-7b62-4fbf-aab6-ada1df8794b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 10,
                "x": 128,
                "y": 80
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "6c26b5dc-336d-49da-896c-728797bc20ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 16,
                "y": 80
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "53b10fed-ab40-4d6d-9edd-cf9c3ba398c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 5,
                "x": 241,
                "y": 54
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "b8835e73-3657-4328-b63c-e0033908f389",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 227,
                "y": 54
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "1a3b4b0a-905b-40cf-bc36-6066d2451bfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 5,
                "x": 220,
                "y": 54
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "32242bd5-7809-4c7b-bb6f-6d21bf7a5bcd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 10,
                "x": 208,
                "y": 54
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "9248d867-de2a-4256-a050-9859e6f4d16c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 194,
                "y": 54
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "7f29faa9-7bd4-40bc-817b-504b6ded9782",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 24,
                "offset": 3,
                "shift": 14,
                "w": 8,
                "x": 184,
                "y": 54
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "b046f274-bbbf-4616-ac4e-4170e11c9d8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 170,
                "y": 54
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "ce36b70b-e115-47a1-9a56-54b55b16bb8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 156,
                "y": 54
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "b7bdbe1d-68ff-4639-88af-7540ebad801c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 142,
                "y": 54
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "00ccd51e-987b-4438-89bc-fad17b4b6236",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 2,
                "y": 80
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "27c410b7-d162-4801-aab9-3926fe5dc2cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 140,
                "y": 80
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "6918c80f-ed0f-429e-84f6-4022eae1ef3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 154,
                "y": 80
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "bf7e79f6-eaff-4e46-ab32-c5b69fe1c03b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 168,
                "y": 80
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "fe039a65-ceee-44f5-8640-9c7e5aa76999",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 196,
                "y": 106
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "02d939fa-0de8-4179-9a62-7171dd1e282a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 24,
                "offset": 5,
                "shift": 14,
                "w": 4,
                "x": 190,
                "y": 106
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "825e907c-5a45-4153-8588-4426dd1c26f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 24,
                "offset": 5,
                "shift": 14,
                "w": 4,
                "x": 184,
                "y": 106
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "6b52d3ba-69aa-4f25-8149-82348cb302e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 170,
                "y": 106
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "6da5c612-8eb1-4519-b0d9-909380da5de4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 156,
                "y": 106
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "b94f2d25-4b62-4fe8-b0d8-2c72756703f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 142,
                "y": 106
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "0fb74c45-98a6-467e-829a-1c0937d35fcf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 128,
                "y": 106
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "9f67f91c-21ca-4e3c-940f-70eaf8eccbbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 114,
                "y": 106
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "06247353-bb41-4cb1-9aea-801f852912af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 100,
                "y": 106
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "1741207c-1cfb-4c49-8026-b864b22b5a42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 86,
                "y": 106
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "a7b32053-df75-41a7-a2a4-efc6b8401ce9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 72,
                "y": 106
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "fb87c836-0c00-4802-9c18-571e983b044a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 58,
                "y": 106
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "97e76b22-16d7-4077-b7ed-27a499d0d345",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 44,
                "y": 106
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "c795be6b-1914-44c4-a3db-d158cf535484",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 30,
                "y": 106
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "dacb7b91-8b50-4505-a55a-3cd21f773d56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 16,
                "y": 106
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "4d0f582f-b762-46ee-b185-bcefb884f97d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 2,
                "y": 106
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "732f79d6-607e-4fd9-a485-c72877281144",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 24,
                "offset": 5,
                "shift": 14,
                "w": 4,
                "x": 238,
                "y": 80
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "b1e34e4c-4f1e-4afb-91c9-e6ac928fdd5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 224,
                "y": 80
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "b904b17d-7c50-474e-945e-8e905ad4d384",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 210,
                "y": 80
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "b8fb19e9-b300-4514-b330-8751e19e3e0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 196,
                "y": 80
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "b2d5c679-a2fb-4642-9ee4-baaf91cedf55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 182,
                "y": 80
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "7ea8c718-778a-4582-82ea-633b43ccc3ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 128,
                "y": 54
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "fd4225f4-5a04-4200-93fb-f80a6b69a4e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 114,
                "y": 54
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "5e3b6fca-4d54-497b-bfea-16149a7af583",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 100,
                "y": 54
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "17f976d6-3525-44cd-91b9-1d76c212b3a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 52,
                "y": 28
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "81bb6e57-31ad-4def-a87e-78794511d00e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 30,
                "y": 28
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "f82fa509-5806-4bb8-9900-0726210e25b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 16,
                "y": 28
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "7c02588e-4751-4a65-afd7-df4d8f2f01bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 2,
                "y": 28
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "6750cc91-43f9-479d-8d9c-1843e84d6b6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 234,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "cb55aab4-e8da-4676-b1cc-b1cccd994fab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 220,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "38dc8242-9178-4833-a1f7-0a7015a2fd98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 206,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "a6dad401-6fe2-4c6d-89a7-3fbf70ceafce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 192,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "60e065bc-2e8d-4531-a3f4-02bb88ae077b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 178,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "53ff5626-c3b0-484c-a487-3075c6b9fe4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 164,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "71191844-d1b6-46fc-818d-5e8fda31cf6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 24,
                "offset": 5,
                "shift": 14,
                "w": 6,
                "x": 44,
                "y": 28
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "c2e54b86-e21d-41e7-847e-471faec10b15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 24,
                "offset": 3,
                "shift": 14,
                "w": 10,
                "x": 152,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "3b504b2f-4c5d-4f4a-88c0-49f9d45c876d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 24,
                "offset": 3,
                "shift": 14,
                "w": 6,
                "x": 130,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "7d35cadd-761f-4c74-8c0b-424172c60c11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 116,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "3d59f42b-893f-468c-98f4-3147ff1298f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 102,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "28425332-03e7-45c8-9a60-c294e4768f78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 88,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "b258e8bf-f3c6-47b5-9f64-27abb1a0d7b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 74,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "7da39eda-f6ca-4d6c-80bf-88d57f158b0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 60,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "ad29ba20-beee-4070-80bb-06e412fbf795",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 46,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "ef704f9b-5788-4e3c-bfc4-a4f7f96f454b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 32,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "fe176e23-ffd6-477c-a9dd-dd598d3913b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 18,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "6170a98a-374a-41be-8bb2-02636ba5b8c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 138,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "4d4173fb-3b64-4a68-a93d-f73a93e10b89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 66,
                "y": 28
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "69f16aea-f9c5-48bd-969e-ef56f918090e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 196,
                "y": 28
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "424ecc26-eae0-4a29-9606-936a6b890b32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 24,
                "offset": 5,
                "shift": 14,
                "w": 4,
                "x": 80,
                "y": 28
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "a2f469d7-afde-47d7-a093-b8ae3d5a3e35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 24,
                "offset": 3,
                "shift": 14,
                "w": 6,
                "x": 78,
                "y": 54
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "ac330cf6-cd07-40da-8a13-ff7343e8651a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 64,
                "y": 54
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "68d310e5-eb98-4c6b-9148-096b98c0755c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 24,
                "offset": 5,
                "shift": 14,
                "w": 4,
                "x": 58,
                "y": 54
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "cd08d43c-6067-44be-9214-03cffc4abd19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 44,
                "y": 54
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "a1bbd8b3-593e-4203-a225-440b299b35d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 30,
                "y": 54
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "97929eed-b498-43c1-949d-cb8844267ecc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 16,
                "y": 54
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "4285bcff-7a2d-4f86-8d44-3220f4a02f73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 2,
                "y": 54
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "8098dfc1-9c11-4063-b67a-6fa8768ebe92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 232,
                "y": 28
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "c75e510b-49e3-493f-affd-e4deaa67e223",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 10,
                "x": 220,
                "y": 28
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "d988ddf4-1bfc-482d-b81a-07bd373ebd1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 86,
                "y": 54
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "cb574c3b-eec7-499b-9b4d-5e4e6fe962d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 24,
                "offset": 3,
                "shift": 14,
                "w": 8,
                "x": 210,
                "y": 28
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "605d95b0-1adc-4eb5-90ee-51391070aac7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 182,
                "y": 28
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "bd97f890-27c4-4ba0-a988-ad82a35e5532",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 168,
                "y": 28
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "1005a226-54d1-4604-a319-9d9e1977e62f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 154,
                "y": 28
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "3fc4b2e8-9bbf-4e35-b033-182488442951",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 140,
                "y": 28
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "749d7a8e-f479-45c7-9de1-61a57e319e54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 126,
                "y": 28
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "c4ff4e2e-45ce-41bc-a3a8-556b95e01dbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 112,
                "y": 28
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "bd8ef0af-3dac-46d2-8b9b-9b9e26b815ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 24,
                "offset": 3,
                "shift": 14,
                "w": 8,
                "x": 102,
                "y": 28
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "d128d547-cbc0-4da7-ab75-c8fd44016384",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 24,
                "offset": 5,
                "shift": 14,
                "w": 4,
                "x": 96,
                "y": 28
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "cc29a4b3-6905-4806-9088-ae2cfe8a2164",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 24,
                "offset": 3,
                "shift": 14,
                "w": 8,
                "x": 86,
                "y": 28
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "d19a5187-312c-4a3b-9bdb-b2f26615bd31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 210,
                "y": 106
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "6be88ecb-9a88-4c67-9a0d-21cd8fea8f8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 24,
                "offset": 4,
                "shift": 20,
                "w": 13,
                "x": 224,
                "y": 106
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 16,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}