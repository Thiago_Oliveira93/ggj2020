{
    "id": "42340748-0b51-432d-a5aa-1b05064d9daf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_health_up",
    "eventList": [
        {
            "id": "8f654b3b-0863-447a-b1c2-756f1d798ecc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "8044f7a1-9533-4c43-9ba4-ae399170f7d4",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "42340748-0b51-432d-a5aa-1b05064d9daf"
        },
        {
            "id": "94d2a0d6-f997-4161-927a-b5ab92bfb2d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "42340748-0b51-432d-a5aa-1b05064d9daf"
        },
        {
            "id": "af513936-2ab8-4bbc-9dd2-f0d6d279c370",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "42340748-0b51-432d-a5aa-1b05064d9daf"
        }
    ],
    "maskSpriteId": "88a52b5e-2720-470e-854f-533454abe20f",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b169d6cc-104a-4490-9bfb-984c9f9f058e",
    "visible": true
}