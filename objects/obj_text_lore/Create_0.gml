/// @description Insert description here
// You can write your code in this editor

x = 50
y = 250
timer = 0
durationTime = 2
textpage = 0
separation_v = 48
max_textpage = 12
depth = -200
text = "In the beginning, he made a perfect world and dwelt among us with peace, kindness and love. But darkness also existed and his soldiers insisted on destroying everything that was good in the world. Wars, famine and terror spread and the creator could not let that happen ... now he sets out to repair what has been destroyed."

showKeyText = 0
textKey = "press any key..."
timerBlink = 0;
increase = 1;
max_time = 50
