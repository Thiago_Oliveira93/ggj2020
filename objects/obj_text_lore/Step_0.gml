/// @description Insert description here
// You can write your code in this editor

timer+=0.4

if round(timer) >= string_length(text)
{
	if keyboard_check_pressed(vk_anykey)
		room_goto_next()
	
	showKeyText = 1
	timerBlink += increase;

	//calculations
	if(timerBlink >= max_time || timerBlink <= 0)
	{
		increase = -increase
	}
}

if round(timer) mod 2 == 0
{
	if timer < string_length(text)
	{
		audio_play_sound(snd_blip_text,1,false)
		audio_sound_pitch(snd_blip_text,1.2)
	}
}
