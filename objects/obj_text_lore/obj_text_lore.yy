{
    "id": "ae828ab8-9603-4ba8-805c-e6b784ed7de1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_text_lore",
    "eventList": [
        {
            "id": "03575f62-a1bd-469a-8a19-764966ed8daa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ae828ab8-9603-4ba8-805c-e6b784ed7de1"
        },
        {
            "id": "217b6858-7cb8-4a2d-8266-7227bbbeb822",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "ae828ab8-9603-4ba8-805c-e6b784ed7de1"
        },
        {
            "id": "cf8c9509-ac2e-43e5-9f7b-18f1f48e0dc1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ae828ab8-9603-4ba8-805c-e6b784ed7de1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}