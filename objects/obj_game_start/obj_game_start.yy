{
    "id": "8d297823-eb3c-4c90-a395-9e85d4781cac",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_game_start",
    "eventList": [
        {
            "id": "39a92524-43d6-40ad-b4bf-faa1cf60ed1a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8d297823-eb3c-4c90-a395-9e85d4781cac"
        },
        {
            "id": "4b2f55eb-eb17-40d3-b515-7514c8e7d702",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "8d297823-eb3c-4c90-a395-9e85d4781cac"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3f725215-5e51-416f-8b52-dcb5b92d1ce4",
    "visible": true
}