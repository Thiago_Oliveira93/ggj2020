{
    "id": "96417cf8-a9e8-464c-b2f6-2afb173fc3ef",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_circle_of_light",
    "eventList": [
        {
            "id": "73daa9a0-344d-4c9f-a9a0-0c4db962c2c7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "96417cf8-a9e8-464c-b2f6-2afb173fc3ef"
        },
        {
            "id": "6d320ff5-da08-4553-a162-f9a33fd375ab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "96417cf8-a9e8-464c-b2f6-2afb173fc3ef"
        },
        {
            "id": "c430b7f0-75a3-4732-85ac-0e9122c872b9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "96417cf8-a9e8-464c-b2f6-2afb173fc3ef"
        },
        {
            "id": "5841c07a-b206-4201-ab0a-efbe905855f4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "96417cf8-a9e8-464c-b2f6-2afb173fc3ef"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}