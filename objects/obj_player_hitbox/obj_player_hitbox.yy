{
    "id": "62d17152-2f27-4fa8-95ad-f941edccd391",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player_hitbox",
    "eventList": [
        {
            "id": "13cafcfc-ef82-4ebf-a227-cd61b12dd606",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "62d17152-2f27-4fa8-95ad-f941edccd391"
        },
        {
            "id": "afd48188-1484-4613-b222-4bf6b9a5230f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "62d17152-2f27-4fa8-95ad-f941edccd391"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "cc5884cc-d684-470d-a0fa-f03f44a0cf6b",
    "visible": false
}