/// @description Insert description here
// You can write your code in this editor

if keyboard_check_pressed(vk_escape)
{
	game_restart()
}

if ( state == GAMESTATE.IDLE )
{
	if instance_exists(obj_player)
	{
		global.INPUTSYSTEM = true
	
		if (keyboard_check_pressed(vk_anykey) and obj_player.Moved)
		{
			if instance_exists(obj_enemy) 
			{
				obj_enemy.state = ENEMYSTATE.CHECKING
				state = GAMESTATE.TURN
			}
		}
	}
}

if ( state == GAMESTATE.TURN )
{
	global.INPUTSYSTEM = false
	if instance_exists(obj_enemy)
	{
		if ( obj_enemy.state != ENEMYSTATE.CHECKING )
		{
			state = GAMESTATE.IDLE
		}
	}
}
