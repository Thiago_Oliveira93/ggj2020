{
    "id": "b5faec97-7bdb-4b99-abf2-be0ab9e607d6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_gamelogic",
    "eventList": [
        {
            "id": "d4ee3a97-e9f3-4f84-a961-92cd79cbc861",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b5faec97-7bdb-4b99-abf2-be0ab9e607d6"
        },
        {
            "id": "78cf27de-9463-4399-9627-f625fb4260fd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "b5faec97-7bdb-4b99-abf2-be0ab9e607d6"
        },
        {
            "id": "b6fd7d35-54b3-4fda-871e-c785ed5fc696",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b5faec97-7bdb-4b99-abf2-be0ab9e607d6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0896fc0f-7043-403f-816a-86c73d1b8ab2",
    "visible": true
}