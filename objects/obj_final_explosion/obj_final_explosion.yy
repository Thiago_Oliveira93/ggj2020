{
    "id": "2653246d-e028-4781-99e7-8fd99f66449d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_final_explosion",
    "eventList": [
        {
            "id": "aa6f775b-ce66-4e8f-a8ce-eb33ee979bf9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2653246d-e028-4781-99e7-8fd99f66449d"
        },
        {
            "id": "cf661776-5bb2-4e14-9b87-8ad824cd1f24",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2653246d-e028-4781-99e7-8fd99f66449d"
        },
        {
            "id": "7326787b-3d7c-4732-b5c9-17f2a68cb00d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "2653246d-e028-4781-99e7-8fd99f66449d"
        },
        {
            "id": "1b8ebccb-a8e2-4939-9b76-2de7c87872e9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "2653246d-e028-4781-99e7-8fd99f66449d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0c0347aa-bae6-4a4a-b891-b1e8ca79be23",
    "visible": true
}