{
    "id": "c09f62d5-55fe-460b-84b1-abc13abf7741",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_light_effect",
    "eventList": [
        {
            "id": "e91412fb-e438-409c-8b99-078f3aa15124",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c09f62d5-55fe-460b-84b1-abc13abf7741"
        },
        {
            "id": "95beee2e-7b4d-411e-abe0-38c4bb8c6f68",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c09f62d5-55fe-460b-84b1-abc13abf7741"
        },
        {
            "id": "a0d77cad-39fe-420a-b958-4223dbf53a12",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c09f62d5-55fe-460b-84b1-abc13abf7741"
        },
        {
            "id": "ddbee4a5-3b86-4d75-a42d-7623a63d7e88",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "c09f62d5-55fe-460b-84b1-abc13abf7741"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}