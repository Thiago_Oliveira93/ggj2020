{
    "id": "420e4825-6e68-4cfa-8d19-514bc61edb4f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_explosion",
    "eventList": [
        {
            "id": "95c486af-94e0-4d2d-9408-8afed9acd91b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "420e4825-6e68-4cfa-8d19-514bc61edb4f"
        },
        {
            "id": "8c830218-91e2-44b6-bfa2-86c97a33510f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "420e4825-6e68-4cfa-8d19-514bc61edb4f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e1ccbabd-aa4b-404a-a1f2-48b4ad5ae710",
    "visible": true
}