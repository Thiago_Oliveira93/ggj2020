/// @description Insert description here
// You can write your code in this editor
scr_player_combat()
depth = -600
shadow.x = lerp(sprite_holder.x,x,0.1)
shadow.y =lerp(sprite_holder.y,y,0.1)+6
shadow.depth = depth+1

///cria um objeto q segura o sprite original , movimento suave

sprite_holder.x = lerp(sprite_holder.x,x,0.2)
sprite_holder.y = lerp(sprite_holder.y,y,0.2)
sprite_holder.visible = true
sprite_holder.image_index = image_index
sprite_holder.sprite_index = sprite_index
sprite_holder.depth = depth

if state == "idle"
{
	if round(sprite_holder.x) != round(x) or round(sprite_holder.y) != round(y)
	{
		if sprite_holder.x < x
		{
			sprite_index = spr_player_walk_right
			last_sprite = sprite_index
		}
		else
		{
			sprite_index = spr_player_walk_left
			last_sprite = sprite_index
		}
		image_speed =  1
	}
	if round(sprite_holder.x)== round(x) and round(sprite_holder.y)==round(y)
	{
		sprite_index = last_sprite
		image_index = 0
		image_speed =  0
	}
}
else
{
	if alarm[2] > 0
	{
		state = "attack"
	}
	
	if state == "attack"
	{
		if instance_exists(obj_enemy)
		{
			enemy_nerest = instance_nearest(x,y,obj_enemy)
		}
		if enemy_nerest.x > x
		{
			sprite_index = spr_player_attack
		}
		else
		{
			sprite_index = spr_player_attack_left
			last_sprite = spr_player_walk_left
		}
		if alarm[2]  == 1
		{
			audio_play_sound(snd_player_hit,1,false)
			audio_sound_pitch(snd_player_hit,random_range(0.8,1.2))
		}
		image_speed = 2
	}
}

if hp > 100
{
	hp = 100
}

hsp = 0
vsp = 0

if global.INPUTSYSTEM==false exit;

if keyboard_check_pressed(ord("A"))
{
	hsp=-24
	audio_play_sound(snd_player_walk,1,false)
	audio_sound_pitch(snd_player_walk,random_range(0.7,1.2))
}
else if keyboard_check_pressed(ord("D"))
{
	hsp=24
	audio_play_sound(snd_player_walk,1,false)
		audio_sound_pitch(snd_player_walk,random_range(0.7,1.2))
}
else if keyboard_check_pressed(ord("S"))
{
	vsp=24
	audio_play_sound(snd_player_walk,1,false)
		audio_sound_pitch(snd_player_walk,random_range(0.7,1.2))
}
else if keyboard_check_pressed(ord("W"))
{
	vsp=-24
	audio_play_sound(snd_player_walk,1,false)
		audio_sound_pitch(snd_player_walk,random_range(0.7,1.2))
}

scr_move_and_collide()

////hitbox following
hitbox_up.y = y-24
hitbox_up.x = x

hitbox_down.y = y+24
hitbox_down.x = x

hitbox_right.x = x+24
hitbox_right.y = y

hitbox_left.x = x-24
hitbox_left.y = y

if timer==1
{
	obj_camera.alarm[0] = 5
}

if !place_meeting(x,y,obj_enemy)
{
	timer = 0
}