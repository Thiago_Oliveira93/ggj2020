{
    "id": "8044f7a1-9533-4c43-9ba4-ae399170f7d4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "06f50c61-437b-4acf-b5ca-db1a56db7635",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8044f7a1-9533-4c43-9ba4-ae399170f7d4"
        },
        {
            "id": "ec88cbdb-a748-48d7-bee5-6f65e80544b3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8044f7a1-9533-4c43-9ba4-ae399170f7d4"
        },
        {
            "id": "cb829657-faaf-4c17-90f5-5b7b0541f446",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "8044f7a1-9533-4c43-9ba4-ae399170f7d4"
        },
        {
            "id": "54e0ec99-e823-48d4-a475-abdfb70af149",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8044f7a1-9533-4c43-9ba4-ae399170f7d4"
        },
        {
            "id": "4119e4a9-5b1f-414b-b1b5-b2a1f938ef25",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "8044f7a1-9533-4c43-9ba4-ae399170f7d4"
        },
        {
            "id": "c5153e30-8642-44f0-a2f7-33a63615a257",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "8044f7a1-9533-4c43-9ba4-ae399170f7d4"
        },
        {
            "id": "66191809-c7b3-4a6a-ad63-5baa2970a2ad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "7aa0a9cb-e1bb-42e0-9ddb-2286e0f4e8f2",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "8044f7a1-9533-4c43-9ba4-ae399170f7d4"
        }
    ],
    "maskSpriteId": "cc5884cc-d684-470d-a0fa-f03f44a0cf6b",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "cc5884cc-d684-470d-a0fa-f03f44a0cf6b",
    "visible": false
}