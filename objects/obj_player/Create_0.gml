/// @description Insert description here
// You can write your code in this editor
audio_stop_all()
audio_play_sound(msc_play_ai,1,true)
camera = instance_create_depth(x,y,depth,obj_camera)
shadow = instance_create_depth(x,y,depth+1,obj_shadow)

hp = 100
collider = 0

sprite_holder = instance_create_depth(x,y,depth,obj_player_sprite_holder)
sprite_holder.sprite_index = spr_player
sprite_index = spr_player_walk_right
enemy_nerest = instance_nearest(x,y,obj_enemy)
///movespeed grid based
movespeed = 24
playable = false
last_sprite = sprite_index

hitbox_up = instance_create_depth(x,y-24,depth,obj_player_hitbox)
hitbox_down = instance_create_depth(x,y-24,depth,obj_player_hitbox)
hitbox_right  = instance_create_depth(x,y-24,depth,obj_player_hitbox)
hitbox_left = instance_create_depth(x,y-24,depth,obj_player_hitbox)

///auxilia para q o movimento ocorra uma vez
time_moving = 0
Kup = keyboard_check_pressed(ord("W"))
Kleft = keyboard_check_pressed(ord("A"))
Kdown = keyboard_check_pressed(ord("S"))
Kright = keyboard_check_pressed(ord("D"))
hsp = 0
vsp = 0

state = "idle"

Moved = false
timer = 0