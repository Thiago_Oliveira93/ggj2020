{
    "id": "82396cb1-7878-4f15-8b5c-2b290ae36327",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_text_box",
    "eventList": [
        {
            "id": "50d3e8ca-4f6f-4f6c-9cdd-5f758a86c411",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "82396cb1-7878-4f15-8b5c-2b290ae36327"
        },
        {
            "id": "77b72b77-f9c9-4bed-93bc-20bd6cc11120",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "82396cb1-7878-4f15-8b5c-2b290ae36327"
        },
        {
            "id": "255eb759-9543-42fe-9636-5026c131f1e0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "82396cb1-7878-4f15-8b5c-2b290ae36327"
        },
        {
            "id": "57aed959-a2c1-41e4-a9f2-e660e540ba09",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "82396cb1-7878-4f15-8b5c-2b290ae36327"
        },
        {
            "id": "e47cc927-34be-47a8-98d9-94cc5cc223d9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "82396cb1-7878-4f15-8b5c-2b290ae36327"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "b7a1380f-1134-4fb8-9f5a-cd5f1953f455",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "4ffb861d-911d-4803-9e95-bc0bf40d16f4",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 280,
            "y": 0
        },
        {
            "id": "2959ac65-f24e-4ecc-aa61-6a6fe0dafc44",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 280,
            "y": 56
        },
        {
            "id": "94560301-9935-41ee-a877-a7ca04cd1824",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 56
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "78943f55-f964-4e3a-b67c-47ecf97a7db1",
    "visible": true
}