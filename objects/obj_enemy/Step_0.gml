/// @description Insert description here
// You can write your code in this editor
if path =! noone
{
	path_set_kind(path,false)
}
shadow.x = x-13
shadow.y = y-6
shadow.depth = depth+1


sprite_holder.x = lerp(sprite_holder.x,x,0.2)
sprite_holder.y = lerp(sprite_holder.y,y,0.2)
sprite_holder.visible = true
sprite_holder.image_index = image_index
sprite_holder.sprite_index = sprite_index
sprite_holder.depth = depth


if obj_gamelogic.state == GAMESTATE.GAMEOVER exit;

if ( birl ) state = ENEMYSTATE.ALERT;
	
if (state == ENEMYSTATE.IDLE)
{
}

if (state == ENEMYSTATE.CHECKING)
{	
	distanceToPlayer = distance_to_object(obj_player)
	
	if ( distanceToPlayer < cell_height * 6 && !collision_line(x,y, obj_player.x, obj_player.y, obj_solid, false, true) )
	{
		state = ENEMYSTATE.ALERT	
		birl = true
		
		exit
	}
	
	state = ENEMYSTATE.IDLE
}

if (state == ENEMYSTATE.ALERT)
{
	if ( obj_gamelogic.state == GAMESTATE.TURN )
	{ 
		path_speed_ = 24
		scr_path_finding()
	}
	else
	{
		path_speed_ = 0
		path_end()
	}
}

if hp <= 0 and obj_player.alarm[2] == -1
{
	instance_destroy()
}
