{
    "id": "7aa0a9cb-e1bb-42e0-9ddb-2286e0f4e8f2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemy",
    "eventList": [
        {
            "id": "7ec6664b-ca8e-40b6-9761-07c28b6f965c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7aa0a9cb-e1bb-42e0-9ddb-2286e0f4e8f2"
        },
        {
            "id": "f069f31a-27e4-4f05-af46-edeb51e8d571",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7aa0a9cb-e1bb-42e0-9ddb-2286e0f4e8f2"
        },
        {
            "id": "293f3686-bf65-4c2c-a868-e534c454ba3a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "7aa0a9cb-e1bb-42e0-9ddb-2286e0f4e8f2"
        },
        {
            "id": "369f6f1e-0713-4704-a9e7-5857a0123343",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "7aa0a9cb-e1bb-42e0-9ddb-2286e0f4e8f2"
        },
        {
            "id": "2ba900a0-5a06-4478-aaac-552724bb9fbe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "8044f7a1-9533-4c43-9ba4-ae399170f7d4",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "7aa0a9cb-e1bb-42e0-9ddb-2286e0f4e8f2"
        },
        {
            "id": "81bfd68a-8300-4d79-aad0-fedc8adb6b85",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "7aa0a9cb-e1bb-42e0-9ddb-2286e0f4e8f2"
        }
    ],
    "maskSpriteId": "0896fc0f-7043-403f-816a-86c73d1b8ab2",
    "overriddenProperties": null,
    "parentObjectId": "6a54f616-9903-442c-9b05-906bf2b53749",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0896fc0f-7043-403f-816a-86c73d1b8ab2",
    "visible": true
}