{
    "id": "a11c9fe0-06c7-4450-be8c-ce7302c638bb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_altar",
    "eventList": [
        {
            "id": "efb045ce-0858-42f6-83ed-3d0546669ef0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a11c9fe0-06c7-4450-be8c-ce7302c638bb"
        },
        {
            "id": "ff87bd9f-5935-422d-bd5a-f932c5a5b0ff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a11c9fe0-06c7-4450-be8c-ce7302c638bb"
        },
        {
            "id": "095ebe45-2050-4ff0-bb09-0f94be83d417",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "8044f7a1-9533-4c43-9ba4-ae399170f7d4",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "a11c9fe0-06c7-4450-be8c-ce7302c638bb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4ab75a60-de38-4683-9bed-7e40fb2def0e",
    "visible": true
}