{
    "id": "6b53e3f7-e380-4dfa-8cef-bbd27ddfe1a1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shadow",
    "eventList": [
        {
            "id": "7d328dfa-06e9-4346-a2e8-42179c77dafd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6b53e3f7-e380-4dfa-8cef-bbd27ddfe1a1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4cefdf3c-7dde-41af-becc-fa16b687f9e8",
    "visible": true
}