{
    "id": "bbaf5562-487c-42ad-880a-8c145adca945",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_camera",
    "eventList": [
        {
            "id": "73a97bd9-ef48-45fc-a9a3-22553630ae5c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bbaf5562-487c-42ad-880a-8c145adca945"
        },
        {
            "id": "11abf8ad-a54e-43b1-bac1-321d991e9038",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "bbaf5562-487c-42ad-880a-8c145adca945"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "52e3d601-94c1-4cd0-8df1-92cd0c962318",
    "visible": false
}