{
    "id": "60b195f7-b08c-47f8-ad39-2228742a6a55",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_walk_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 1,
    "bbox_right": 20,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a6c4d3d4-819b-437b-af69-a7559fee9e5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60b195f7-b08c-47f8-ad39-2228742a6a55",
            "compositeImage": {
                "id": "7f4bf4a3-c90d-45c1-ab8b-11b1ed14cc9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6c4d3d4-819b-437b-af69-a7559fee9e5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f3ed829-aa9d-48fb-88a4-ab5fad7d56c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6c4d3d4-819b-437b-af69-a7559fee9e5f",
                    "LayerId": "8b8d62a2-de65-48e5-b199-6e1a13b32279"
                }
            ]
        },
        {
            "id": "308895be-a6f5-488d-a38c-b286800f201f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60b195f7-b08c-47f8-ad39-2228742a6a55",
            "compositeImage": {
                "id": "853a9a7f-d547-453e-8cc9-e213d1eddf4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "308895be-a6f5-488d-a38c-b286800f201f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "540b8cbd-a82e-41a8-9504-ee2959f1eaf9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "308895be-a6f5-488d-a38c-b286800f201f",
                    "LayerId": "8b8d62a2-de65-48e5-b199-6e1a13b32279"
                }
            ]
        },
        {
            "id": "6dfd1be1-f489-4587-8797-f75ccdf44d47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60b195f7-b08c-47f8-ad39-2228742a6a55",
            "compositeImage": {
                "id": "d41ff09f-1de3-437c-8d64-2a37886c678a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6dfd1be1-f489-4587-8797-f75ccdf44d47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ca01996-dd04-40eb-8ae7-ae462d0de0d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6dfd1be1-f489-4587-8797-f75ccdf44d47",
                    "LayerId": "8b8d62a2-de65-48e5-b199-6e1a13b32279"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "8b8d62a2-de65-48e5-b199-6e1a13b32279",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "60b195f7-b08c-47f8-ad39-2228742a6a55",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}