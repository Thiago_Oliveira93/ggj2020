{
    "id": "76bdb4e4-c8ba-4e5c-9d0f-22b1acdb2fb4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_credits",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 215,
    "bbox_left": 0,
    "bbox_right": 379,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "827abbda-19fa-4b76-b4f2-710e6c7e72b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76bdb4e4-c8ba-4e5c-9d0f-22b1acdb2fb4",
            "compositeImage": {
                "id": "f6327425-0c04-4be9-91ef-8dbf0d7aab01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "827abbda-19fa-4b76-b4f2-710e6c7e72b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47fca94e-9688-45e9-b00e-bfed92c31bef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "827abbda-19fa-4b76-b4f2-710e6c7e72b5",
                    "LayerId": "66d00523-023e-4437-bfbc-9c4ae5f3808b"
                }
            ]
        },
        {
            "id": "57563493-f9c4-45b7-9bf2-f73f8e7f97a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76bdb4e4-c8ba-4e5c-9d0f-22b1acdb2fb4",
            "compositeImage": {
                "id": "48a3fa53-3732-4fc9-8dc2-c0c3eb56e7e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57563493-f9c4-45b7-9bf2-f73f8e7f97a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e544fe8-12dc-4fa9-9ef9-c4928a756f56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57563493-f9c4-45b7-9bf2-f73f8e7f97a0",
                    "LayerId": "66d00523-023e-4437-bfbc-9c4ae5f3808b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 216,
    "layers": [
        {
            "id": "66d00523-023e-4437-bfbc-9c4ae5f3808b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "76bdb4e4-c8ba-4e5c-9d0f-22b1acdb2fb4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 380,
    "xorig": 0,
    "yorig": 0
}