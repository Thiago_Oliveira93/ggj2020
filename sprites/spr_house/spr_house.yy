{
    "id": "7dff7402-183b-4000-bd9e-987f65d4a5a7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_house",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 6,
    "bbox_right": 41,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "07ac66f3-cf60-46ba-bd42-d3902e505f46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7dff7402-183b-4000-bd9e-987f65d4a5a7",
            "compositeImage": {
                "id": "81946d3e-26e6-435f-8b8a-5ad48421a5d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07ac66f3-cf60-46ba-bd42-d3902e505f46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3df11d9-d9d9-4519-963a-41eb6d92f0ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07ac66f3-cf60-46ba-bd42-d3902e505f46",
                    "LayerId": "b7194e83-80be-48f8-8df6-d806b305b85b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "b7194e83-80be-48f8-8df6-d806b305b85b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7dff7402-183b-4000-bd9e-987f65d4a5a7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 0,
    "yorig": 0
}