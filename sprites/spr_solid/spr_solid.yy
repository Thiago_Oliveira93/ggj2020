{
    "id": "52e3d601-94c1-4cd0-8df1-92cd0c962318",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_solid",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "56bde394-58eb-4098-ad84-0d9e0036330e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52e3d601-94c1-4cd0-8df1-92cd0c962318",
            "compositeImage": {
                "id": "485657d5-18bd-453f-ab73-b5c53c3186a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56bde394-58eb-4098-ad84-0d9e0036330e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ac1f087-e30c-437a-8311-4f58a40f591a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56bde394-58eb-4098-ad84-0d9e0036330e",
                    "LayerId": "c7baa963-b39c-499e-827f-90c99f1f945d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "c7baa963-b39c-499e-827f-90c99f1f945d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "52e3d601-94c1-4cd0-8df1-92cd0c962318",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}