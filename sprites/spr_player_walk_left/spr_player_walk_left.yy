{
    "id": "0ec9d152-682a-4664-b6bf-e6bd246da9c1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_walk_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 3,
    "bbox_right": 22,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0cb5d6b7-51e3-47c1-a61e-1d970d5a9d93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ec9d152-682a-4664-b6bf-e6bd246da9c1",
            "compositeImage": {
                "id": "78680499-ee51-43a6-a5c3-0e85ccd66f22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0cb5d6b7-51e3-47c1-a61e-1d970d5a9d93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0eec789-272a-46bf-8478-1a5f67c72306",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0cb5d6b7-51e3-47c1-a61e-1d970d5a9d93",
                    "LayerId": "810e1346-c88d-4d60-9500-a06da2ba8f1a"
                }
            ]
        },
        {
            "id": "41a0b49e-7ffd-4cca-a2f5-1c73447dca20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ec9d152-682a-4664-b6bf-e6bd246da9c1",
            "compositeImage": {
                "id": "a2c26809-ae82-4af3-82b0-4c334e306497",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41a0b49e-7ffd-4cca-a2f5-1c73447dca20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26e1ecd8-a2b9-4584-9691-4230947db693",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41a0b49e-7ffd-4cca-a2f5-1c73447dca20",
                    "LayerId": "810e1346-c88d-4d60-9500-a06da2ba8f1a"
                }
            ]
        },
        {
            "id": "a6a6084e-703c-4e88-861e-6bbf1a6abc54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ec9d152-682a-4664-b6bf-e6bd246da9c1",
            "compositeImage": {
                "id": "71266a98-2155-4e42-adec-afaec7975b8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6a6084e-703c-4e88-861e-6bbf1a6abc54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec13f873-056c-49e6-ae4e-f9c1da0f0c47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6a6084e-703c-4e88-861e-6bbf1a6abc54",
                    "LayerId": "810e1346-c88d-4d60-9500-a06da2ba8f1a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "810e1346-c88d-4d60-9500-a06da2ba8f1a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0ec9d152-682a-4664-b6bf-e6bd246da9c1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}