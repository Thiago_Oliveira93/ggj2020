{
    "id": "9967c249-3c43-4111-afae-df4d698ce2cc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_attack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 1,
    "bbox_right": 22,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fdf969f3-1855-4b3e-b506-f2f3596edf13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9967c249-3c43-4111-afae-df4d698ce2cc",
            "compositeImage": {
                "id": "625164bc-03f6-4c97-a0fe-f73ec203aece",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdf969f3-1855-4b3e-b506-f2f3596edf13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4cbac21-e9a9-47b6-b76b-47aac1503cff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdf969f3-1855-4b3e-b506-f2f3596edf13",
                    "LayerId": "22c5d037-841e-4148-89a6-227e997ea525"
                }
            ]
        },
        {
            "id": "7742acb2-53c8-415e-a17f-bfcfef9e7b19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9967c249-3c43-4111-afae-df4d698ce2cc",
            "compositeImage": {
                "id": "8f0e4924-e579-47f9-a54d-4a9ee3098c45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7742acb2-53c8-415e-a17f-bfcfef9e7b19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c2936c0-4cac-45bf-abd5-90c4d65c1d5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7742acb2-53c8-415e-a17f-bfcfef9e7b19",
                    "LayerId": "22c5d037-841e-4148-89a6-227e997ea525"
                }
            ]
        },
        {
            "id": "9e0d6777-ae3b-4819-a9ff-4266081f21b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9967c249-3c43-4111-afae-df4d698ce2cc",
            "compositeImage": {
                "id": "9061aa0e-30a7-4055-81b1-69df0d4396f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e0d6777-ae3b-4819-a9ff-4266081f21b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4dd0fce1-42f9-46ba-8cee-0ba65d35614a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e0d6777-ae3b-4819-a9ff-4266081f21b5",
                    "LayerId": "22c5d037-841e-4148-89a6-227e997ea525"
                }
            ]
        },
        {
            "id": "df6eec34-82d1-4343-8854-df8b49255038",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9967c249-3c43-4111-afae-df4d698ce2cc",
            "compositeImage": {
                "id": "5deda8a7-caee-4bcf-844b-38d682f0de91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df6eec34-82d1-4343-8854-df8b49255038",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e8c16bb-1e13-4ef2-b0b2-047e219d8909",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df6eec34-82d1-4343-8854-df8b49255038",
                    "LayerId": "22c5d037-841e-4148-89a6-227e997ea525"
                }
            ]
        },
        {
            "id": "1c44615b-d466-40f7-8f01-cb8c73195e22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9967c249-3c43-4111-afae-df4d698ce2cc",
            "compositeImage": {
                "id": "75725362-c59c-4a90-a02e-c7c09c5b348a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c44615b-d466-40f7-8f01-cb8c73195e22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9092ba6-4f1e-4ccb-8e55-38df7bb253d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c44615b-d466-40f7-8f01-cb8c73195e22",
                    "LayerId": "22c5d037-841e-4148-89a6-227e997ea525"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "22c5d037-841e-4148-89a6-227e997ea525",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9967c249-3c43-4111-afae-df4d698ce2cc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}