{
    "id": "78943f55-f964-4e3a-b67c-47ecf97a7db1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_text_box",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 251,
    "bbox_left": 0,
    "bbox_right": 1259,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "676af998-a71f-4841-b607-ee12a279d8d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78943f55-f964-4e3a-b67c-47ecf97a7db1",
            "compositeImage": {
                "id": "efee6302-da77-4045-b892-a7f7b6a231f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "676af998-a71f-4841-b607-ee12a279d8d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9a51e0f-9f3c-4e8e-978a-eadb1a4b8d74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "676af998-a71f-4841-b607-ee12a279d8d2",
                    "LayerId": "c6991d95-8f12-48a0-a034-b48719bf720f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 252,
    "layers": [
        {
            "id": "c6991d95-8f12-48a0-a034-b48719bf720f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "78943f55-f964-4e3a-b67c-47ecf97a7db1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1260,
    "xorig": 0,
    "yorig": 0
}