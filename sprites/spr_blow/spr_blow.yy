{
    "id": "e1ccbabd-aa4b-404a-a1f2-48b4ad5ae710",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_blow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9b229ebf-ce43-4e79-ab3c-86c6d59b2fe1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ccbabd-aa4b-404a-a1f2-48b4ad5ae710",
            "compositeImage": {
                "id": "d6f4029a-8137-4618-bc85-baf8cf072afa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b229ebf-ce43-4e79-ab3c-86c6d59b2fe1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a6366dd-8aac-44b7-90c6-a82719e68f37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b229ebf-ce43-4e79-ab3c-86c6d59b2fe1",
                    "LayerId": "0412f924-09b2-4791-8016-9d9659f30f22"
                }
            ]
        },
        {
            "id": "b16907e0-60a0-4197-9fb0-91f0d0028823",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ccbabd-aa4b-404a-a1f2-48b4ad5ae710",
            "compositeImage": {
                "id": "efb0b50a-6cd7-4c99-9334-2285b88b17e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b16907e0-60a0-4197-9fb0-91f0d0028823",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23a9e30d-f416-474d-be16-b693a6c67d4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b16907e0-60a0-4197-9fb0-91f0d0028823",
                    "LayerId": "0412f924-09b2-4791-8016-9d9659f30f22"
                }
            ]
        },
        {
            "id": "2fe31d9d-0455-46d8-bca1-daa9a43e92e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ccbabd-aa4b-404a-a1f2-48b4ad5ae710",
            "compositeImage": {
                "id": "3b9282cf-4fbd-4abe-aeeb-bfedc681e2f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fe31d9d-0455-46d8-bca1-daa9a43e92e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9de324e-d2a7-4b2a-bcde-ed976375e7aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fe31d9d-0455-46d8-bca1-daa9a43e92e3",
                    "LayerId": "0412f924-09b2-4791-8016-9d9659f30f22"
                }
            ]
        },
        {
            "id": "16fc2a22-00b9-4d81-94f2-f047d2b7ecae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ccbabd-aa4b-404a-a1f2-48b4ad5ae710",
            "compositeImage": {
                "id": "59aa6ca9-9d69-425e-a834-e6d958b1aa94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16fc2a22-00b9-4d81-94f2-f047d2b7ecae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8199e528-a69f-4128-ab89-49bfd9c18eca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16fc2a22-00b9-4d81-94f2-f047d2b7ecae",
                    "LayerId": "0412f924-09b2-4791-8016-9d9659f30f22"
                }
            ]
        },
        {
            "id": "d8a2c2b2-bf5f-43c6-859c-a4fc21592eb6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ccbabd-aa4b-404a-a1f2-48b4ad5ae710",
            "compositeImage": {
                "id": "4a13a7b8-d5b6-4156-b149-3dbd6870ced2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8a2c2b2-bf5f-43c6-859c-a4fc21592eb6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29c4470a-ca74-4bd0-84ba-8b9a846d41be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8a2c2b2-bf5f-43c6-859c-a4fc21592eb6",
                    "LayerId": "0412f924-09b2-4791-8016-9d9659f30f22"
                }
            ]
        },
        {
            "id": "706524ce-bad9-41ed-9b52-f0fa3faf520e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1ccbabd-aa4b-404a-a1f2-48b4ad5ae710",
            "compositeImage": {
                "id": "9becedec-6b7f-4e71-9be9-48c04d07fda0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "706524ce-bad9-41ed-9b52-f0fa3faf520e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d0e86e2-ddcb-4880-8b1e-e56d9ea2fdec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "706524ce-bad9-41ed-9b52-f0fa3faf520e",
                    "LayerId": "0412f924-09b2-4791-8016-9d9659f30f22"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "0412f924-09b2-4791-8016-9d9659f30f22",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e1ccbabd-aa4b-404a-a1f2-48b4ad5ae710",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 12
}