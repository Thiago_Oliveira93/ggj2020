{
    "id": "0c0347aa-bae6-4a4a-b891-b1e8ca79be23",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_explosion",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 0,
    "bbox_right": 459,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a433e561-9c96-48b3-b9ae-2a3f049fc12a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c0347aa-bae6-4a4a-b891-b1e8ca79be23",
            "compositeImage": {
                "id": "21054f57-c9d2-429f-9541-b63b4d44d864",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a433e561-9c96-48b3-b9ae-2a3f049fc12a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb4b7a15-2756-4c7b-ba30-296abb887292",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a433e561-9c96-48b3-b9ae-2a3f049fc12a",
                    "LayerId": "104c3b74-4841-44ea-a5c9-f5c2aad2374a"
                }
            ]
        },
        {
            "id": "34013394-9bd3-4529-8967-af72f2bd4dbe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c0347aa-bae6-4a4a-b891-b1e8ca79be23",
            "compositeImage": {
                "id": "57cd8703-c397-42ed-a83a-54c93e375fe3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34013394-9bd3-4529-8967-af72f2bd4dbe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfd31e98-9c77-415d-ba33-7b2f25fdff16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34013394-9bd3-4529-8967-af72f2bd4dbe",
                    "LayerId": "104c3b74-4841-44ea-a5c9-f5c2aad2374a"
                }
            ]
        },
        {
            "id": "f65251ae-8a86-4296-95d0-9ffb7922cc60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c0347aa-bae6-4a4a-b891-b1e8ca79be23",
            "compositeImage": {
                "id": "14449204-9bb1-4ab9-8137-d581e6460361",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f65251ae-8a86-4296-95d0-9ffb7922cc60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c447e074-df91-4d43-b7b1-005f7bb8d5f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f65251ae-8a86-4296-95d0-9ffb7922cc60",
                    "LayerId": "104c3b74-4841-44ea-a5c9-f5c2aad2374a"
                }
            ]
        },
        {
            "id": "4797e492-b265-4c53-b30c-1eef222d0b78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c0347aa-bae6-4a4a-b891-b1e8ca79be23",
            "compositeImage": {
                "id": "c11c03eb-61df-4f47-a665-eec40afb8151",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4797e492-b265-4c53-b30c-1eef222d0b78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37fe84c6-35bc-439f-9970-f46a34583f9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4797e492-b265-4c53-b30c-1eef222d0b78",
                    "LayerId": "104c3b74-4841-44ea-a5c9-f5c2aad2374a"
                }
            ]
        },
        {
            "id": "241cef79-1746-4af3-83b2-b877916ae8d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c0347aa-bae6-4a4a-b891-b1e8ca79be23",
            "compositeImage": {
                "id": "ddabb424-c01f-4075-8bdf-5f051a4c102c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "241cef79-1746-4af3-83b2-b877916ae8d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66acd912-b480-4e18-9d21-f2ef45df89d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "241cef79-1746-4af3-83b2-b877916ae8d6",
                    "LayerId": "104c3b74-4841-44ea-a5c9-f5c2aad2374a"
                }
            ]
        },
        {
            "id": "57e9137b-c55c-455b-91fd-0a14bdf55961",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c0347aa-bae6-4a4a-b891-b1e8ca79be23",
            "compositeImage": {
                "id": "3026f1a3-84cd-4e58-ac5a-3bf898407d13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57e9137b-c55c-455b-91fd-0a14bdf55961",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3344a547-21ba-438d-b20c-4dcd0998a525",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57e9137b-c55c-455b-91fd-0a14bdf55961",
                    "LayerId": "104c3b74-4841-44ea-a5c9-f5c2aad2374a"
                }
            ]
        },
        {
            "id": "5f257c41-bc5c-4651-a1f2-1c26ed0b6939",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c0347aa-bae6-4a4a-b891-b1e8ca79be23",
            "compositeImage": {
                "id": "c953850a-74eb-479c-b3f2-67f81790551a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f257c41-bc5c-4651-a1f2-1c26ed0b6939",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3195a63-5453-467a-8acf-687d4a8914e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f257c41-bc5c-4651-a1f2-1c26ed0b6939",
                    "LayerId": "104c3b74-4841-44ea-a5c9-f5c2aad2374a"
                }
            ]
        },
        {
            "id": "6462b65f-8480-483e-aff3-bac7dfc4c15b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c0347aa-bae6-4a4a-b891-b1e8ca79be23",
            "compositeImage": {
                "id": "e321a503-4b1f-46a3-8320-f9997091cb7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6462b65f-8480-483e-aff3-bac7dfc4c15b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cdf08fd7-bc59-4328-82ff-30f3451c2758",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6462b65f-8480-483e-aff3-bac7dfc4c15b",
                    "LayerId": "104c3b74-4841-44ea-a5c9-f5c2aad2374a"
                }
            ]
        },
        {
            "id": "e953dcdc-f8a4-454b-9664-e5285f8f004d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c0347aa-bae6-4a4a-b891-b1e8ca79be23",
            "compositeImage": {
                "id": "d256eb40-7977-4c5e-91d0-94d374d28a21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e953dcdc-f8a4-454b-9664-e5285f8f004d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e1f4342-6c46-4579-805c-7b11698a6410",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e953dcdc-f8a4-454b-9664-e5285f8f004d",
                    "LayerId": "104c3b74-4841-44ea-a5c9-f5c2aad2374a"
                }
            ]
        },
        {
            "id": "a9cebd61-8b15-4a4e-8259-56c6146cc736",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c0347aa-bae6-4a4a-b891-b1e8ca79be23",
            "compositeImage": {
                "id": "dc44d5d5-902c-4df2-a384-f4553dfa583d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9cebd61-8b15-4a4e-8259-56c6146cc736",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8c7a365-5d1a-487a-b881-a0c80984ef74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9cebd61-8b15-4a4e-8259-56c6146cc736",
                    "LayerId": "104c3b74-4841-44ea-a5c9-f5c2aad2374a"
                }
            ]
        },
        {
            "id": "b577d2ab-6050-45c3-adab-16fdfa7bf588",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c0347aa-bae6-4a4a-b891-b1e8ca79be23",
            "compositeImage": {
                "id": "458c03d4-207c-48fb-a57e-2e4fa4c8b5b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b577d2ab-6050-45c3-adab-16fdfa7bf588",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c728dc1d-1233-4b65-b3a9-22b6e6380236",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b577d2ab-6050-45c3-adab-16fdfa7bf588",
                    "LayerId": "104c3b74-4841-44ea-a5c9-f5c2aad2374a"
                }
            ]
        },
        {
            "id": "69826c46-3812-475b-80e1-b9366d636dc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c0347aa-bae6-4a4a-b891-b1e8ca79be23",
            "compositeImage": {
                "id": "89530a2f-a6d8-4def-90f2-339637843042",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69826c46-3812-475b-80e1-b9366d636dc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30e96656-22da-473a-b438-6e4c7ff620bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69826c46-3812-475b-80e1-b9366d636dc9",
                    "LayerId": "104c3b74-4841-44ea-a5c9-f5c2aad2374a"
                }
            ]
        },
        {
            "id": "cdf54503-b0e1-4c00-aa3f-5e6fa5fa4dc2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c0347aa-bae6-4a4a-b891-b1e8ca79be23",
            "compositeImage": {
                "id": "cc3500e6-7589-43aa-a564-5bb43705735e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdf54503-b0e1-4c00-aa3f-5e6fa5fa4dc2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5735ad5-cec1-416a-9354-68294b4db390",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdf54503-b0e1-4c00-aa3f-5e6fa5fa4dc2",
                    "LayerId": "104c3b74-4841-44ea-a5c9-f5c2aad2374a"
                }
            ]
        },
        {
            "id": "8516d861-45ca-4776-979b-0e9163529493",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c0347aa-bae6-4a4a-b891-b1e8ca79be23",
            "compositeImage": {
                "id": "d20be12a-f193-4255-b53f-69518d1a0a62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8516d861-45ca-4776-979b-0e9163529493",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08cb5673-5e70-4c2e-85b9-7253c5bb567b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8516d861-45ca-4776-979b-0e9163529493",
                    "LayerId": "104c3b74-4841-44ea-a5c9-f5c2aad2374a"
                }
            ]
        },
        {
            "id": "124bdfa7-795e-46d6-99b1-f141f5925c2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c0347aa-bae6-4a4a-b891-b1e8ca79be23",
            "compositeImage": {
                "id": "b4d0396b-a975-44a3-85c2-605006184578",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "124bdfa7-795e-46d6-99b1-f141f5925c2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d50dbd7-6533-4ea6-ba7a-f6e898177bee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "124bdfa7-795e-46d6-99b1-f141f5925c2c",
                    "LayerId": "104c3b74-4841-44ea-a5c9-f5c2aad2374a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "104c3b74-4841-44ea-a5c9-f5c2aad2374a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0c0347aa-bae6-4a4a-b891-b1e8ca79be23",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 460,
    "xorig": 230,
    "yorig": 149
}