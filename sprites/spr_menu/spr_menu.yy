{
    "id": "8ae08006-f863-407c-b4a4-cb89e5e63d2e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menu",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 767,
    "bbox_left": 0,
    "bbox_right": 1365,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7e4fa5fc-57aa-483c-8b08-3ffad1c1c7b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ae08006-f863-407c-b4a4-cb89e5e63d2e",
            "compositeImage": {
                "id": "1ac28d3c-68bf-46d2-bd92-242247c92fa1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e4fa5fc-57aa-483c-8b08-3ffad1c1c7b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4553742f-1403-48af-acba-b47bc8651eda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e4fa5fc-57aa-483c-8b08-3ffad1c1c7b0",
                    "LayerId": "ebc1d9ee-b6bc-4867-a743-07ac9bddb2d8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 768,
    "layers": [
        {
            "id": "ebc1d9ee-b6bc-4867-a743-07ac9bddb2d8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8ae08006-f863-407c-b4a4-cb89e5e63d2e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1366,
    "xorig": 0,
    "yorig": 0
}