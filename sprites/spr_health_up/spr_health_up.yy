{
    "id": "88a52b5e-2720-470e-854f-533454abe20f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_health_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3a93dedb-8f0f-46fb-8b02-7abd3ecd8f78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "88a52b5e-2720-470e-854f-533454abe20f",
            "compositeImage": {
                "id": "61e638a4-f110-4b7f-b20f-4386c0766381",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a93dedb-8f0f-46fb-8b02-7abd3ecd8f78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc5f2169-8481-44e0-a3f7-fe77932c6446",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a93dedb-8f0f-46fb-8b02-7abd3ecd8f78",
                    "LayerId": "765827f7-533c-438b-a29e-0ce202aa8a93"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "765827f7-533c-438b-a29e-0ce202aa8a93",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "88a52b5e-2720-470e-854f-533454abe20f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}