{
    "id": "40c21b1a-48e3-4767-b21a-445246774a28",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fence",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 3,
    "bbox_right": 22,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f51c7452-a0db-475e-ab78-322597849336",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40c21b1a-48e3-4767-b21a-445246774a28",
            "compositeImage": {
                "id": "7570dca0-d4bc-4d71-a6f3-0ccc8325eab4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f51c7452-a0db-475e-ab78-322597849336",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "077fe1b9-3ed1-4693-9f53-f56c88b92b21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f51c7452-a0db-475e-ab78-322597849336",
                    "LayerId": "e02d57bf-4e9f-46c6-b156-a5974b68b738"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "e02d57bf-4e9f-46c6-b156-a5974b68b738",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "40c21b1a-48e3-4767-b21a-445246774a28",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}