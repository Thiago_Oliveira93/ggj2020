{
    "id": "6b6fa7a3-3d94-4269-8d13-f338c2823032",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_green_enemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 1,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d16aee98-893b-4875-8618-e43f64c08170",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b6fa7a3-3d94-4269-8d13-f338c2823032",
            "compositeImage": {
                "id": "01fb9c9d-1929-45e0-a834-6553dbe6fa72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d16aee98-893b-4875-8618-e43f64c08170",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42706070-2964-4de4-b8ef-e3d1a2b89669",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d16aee98-893b-4875-8618-e43f64c08170",
                    "LayerId": "8a4daafe-1539-4bd0-b9e4-54de737c2028"
                }
            ]
        },
        {
            "id": "5bf48248-aebd-4af3-bd01-5d603bea6145",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b6fa7a3-3d94-4269-8d13-f338c2823032",
            "compositeImage": {
                "id": "85763c5f-5da8-4c3f-9df8-176755d53db9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5bf48248-aebd-4af3-bd01-5d603bea6145",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67f9835a-72b2-4719-a87d-91c15db53ddc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5bf48248-aebd-4af3-bd01-5d603bea6145",
                    "LayerId": "8a4daafe-1539-4bd0-b9e4-54de737c2028"
                }
            ]
        },
        {
            "id": "8ca59c91-30f7-445e-8b9d-11fa8e2ec660",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b6fa7a3-3d94-4269-8d13-f338c2823032",
            "compositeImage": {
                "id": "692739d0-b59a-4837-8c7e-9e9498e05acd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ca59c91-30f7-445e-8b9d-11fa8e2ec660",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d333e4ac-8dfe-4d92-882c-8eefb8cfc1be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ca59c91-30f7-445e-8b9d-11fa8e2ec660",
                    "LayerId": "8a4daafe-1539-4bd0-b9e4-54de737c2028"
                }
            ]
        },
        {
            "id": "634e1493-d4b4-486e-b84a-b10fa01524b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b6fa7a3-3d94-4269-8d13-f338c2823032",
            "compositeImage": {
                "id": "4a982c13-f2db-45c1-b911-4c49cea45e91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "634e1493-d4b4-486e-b84a-b10fa01524b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cafb514-4e9e-44ad-8ecd-0b21e538e70a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "634e1493-d4b4-486e-b84a-b10fa01524b7",
                    "LayerId": "8a4daafe-1539-4bd0-b9e4-54de737c2028"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "8a4daafe-1539-4bd0-b9e4-54de737c2028",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6b6fa7a3-3d94-4269-8d13-f338c2823032",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 12
}