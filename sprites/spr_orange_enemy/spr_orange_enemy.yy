{
    "id": "76ab8671-2ada-48cd-a859-09f5550a7572",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_orange_enemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 1,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "266e4f69-2bfc-473a-978b-eab7085fb6e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76ab8671-2ada-48cd-a859-09f5550a7572",
            "compositeImage": {
                "id": "e2feb4d6-b7c0-43cc-9e92-beed244d9feb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "266e4f69-2bfc-473a-978b-eab7085fb6e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7058eba-7309-4d3e-8e64-6abc056b1229",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "266e4f69-2bfc-473a-978b-eab7085fb6e1",
                    "LayerId": "0258919a-4333-41bd-8e2d-bac302b7d5ac"
                }
            ]
        },
        {
            "id": "4806cf5b-a3b6-43fe-90e5-14ae7ffe2b57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76ab8671-2ada-48cd-a859-09f5550a7572",
            "compositeImage": {
                "id": "e39bea05-da03-47c0-8b1c-b0ca99359c96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4806cf5b-a3b6-43fe-90e5-14ae7ffe2b57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce5936b6-e46b-4465-94bb-eb4fba00f637",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4806cf5b-a3b6-43fe-90e5-14ae7ffe2b57",
                    "LayerId": "0258919a-4333-41bd-8e2d-bac302b7d5ac"
                }
            ]
        },
        {
            "id": "16618d68-0fb1-4e86-b80c-98d4d28b080e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76ab8671-2ada-48cd-a859-09f5550a7572",
            "compositeImage": {
                "id": "8dec6c7c-4100-4138-a027-45f1b0acec73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16618d68-0fb1-4e86-b80c-98d4d28b080e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15763949-e8eb-4b4f-87cd-81fae56d699d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16618d68-0fb1-4e86-b80c-98d4d28b080e",
                    "LayerId": "0258919a-4333-41bd-8e2d-bac302b7d5ac"
                }
            ]
        },
        {
            "id": "2d6020c7-8d62-4dd5-84d8-cef91dd86f72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76ab8671-2ada-48cd-a859-09f5550a7572",
            "compositeImage": {
                "id": "dda99c9f-6fb3-4e47-ad88-2cc31f4909f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d6020c7-8d62-4dd5-84d8-cef91dd86f72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bca0cc2c-576d-49ac-96cc-6a820cd7f2a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d6020c7-8d62-4dd5-84d8-cef91dd86f72",
                    "LayerId": "0258919a-4333-41bd-8e2d-bac302b7d5ac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "0258919a-4333-41bd-8e2d-bac302b7d5ac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "76ab8671-2ada-48cd-a859-09f5550a7572",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 12
}