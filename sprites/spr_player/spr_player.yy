{
    "id": "cc5884cc-d684-470d-a0fa-f03f44a0cf6b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8df25b57-a131-4bef-95d4-817a378beedc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc5884cc-d684-470d-a0fa-f03f44a0cf6b",
            "compositeImage": {
                "id": "9d63a57d-dbb9-4be6-a034-aa4a03896953",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8df25b57-a131-4bef-95d4-817a378beedc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16726cce-62fb-4cd1-b088-4fe9f7699988",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8df25b57-a131-4bef-95d4-817a378beedc",
                    "LayerId": "b1bf0f6d-59d1-4f5f-9a51-20b81775ff0e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "b1bf0f6d-59d1-4f5f-9a51-20b81775ff0e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cc5884cc-d684-470d-a0fa-f03f44a0cf6b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}