{
    "id": "b169d6cc-104a-4490-9bfb-984c9f9f058e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hp_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 1,
    "bbox_right": 24,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2213cbf1-bf71-4e2d-be20-38c2582db0e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b169d6cc-104a-4490-9bfb-984c9f9f058e",
            "compositeImage": {
                "id": "d592fb75-4488-48a7-8c49-a20faa5a32eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2213cbf1-bf71-4e2d-be20-38c2582db0e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cba5625-ef01-484a-a657-2277efdfaa6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2213cbf1-bf71-4e2d-be20-38c2582db0e3",
                    "LayerId": "64c8cd15-c7bf-4a9f-8a96-6b8b57519060"
                }
            ]
        },
        {
            "id": "c324410b-96ba-4c96-a453-3831d5c06031",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b169d6cc-104a-4490-9bfb-984c9f9f058e",
            "compositeImage": {
                "id": "10d058fc-5b31-45af-84b9-60cc484ac147",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c324410b-96ba-4c96-a453-3831d5c06031",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b37d8946-0394-483d-8502-a45cf8ba50b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c324410b-96ba-4c96-a453-3831d5c06031",
                    "LayerId": "64c8cd15-c7bf-4a9f-8a96-6b8b57519060"
                }
            ]
        },
        {
            "id": "1f3ee7d9-3817-4954-94e8-a1b52d464bf5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b169d6cc-104a-4490-9bfb-984c9f9f058e",
            "compositeImage": {
                "id": "b54e61c5-5e14-453c-b2f9-941fab4c69fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f3ee7d9-3817-4954-94e8-a1b52d464bf5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55c86850-085b-422d-96d1-fc96d3539225",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f3ee7d9-3817-4954-94e8-a1b52d464bf5",
                    "LayerId": "64c8cd15-c7bf-4a9f-8a96-6b8b57519060"
                }
            ]
        },
        {
            "id": "4691fa44-3ba2-48a9-86a3-8260306e7a34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b169d6cc-104a-4490-9bfb-984c9f9f058e",
            "compositeImage": {
                "id": "99428418-cd7e-4de9-be9e-807b481f7536",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4691fa44-3ba2-48a9-86a3-8260306e7a34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90424f79-07e8-4601-b8e8-ce6780f7b583",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4691fa44-3ba2-48a9-86a3-8260306e7a34",
                    "LayerId": "64c8cd15-c7bf-4a9f-8a96-6b8b57519060"
                }
            ]
        },
        {
            "id": "048c94da-e99f-430d-bc90-7e8c686e8ccc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b169d6cc-104a-4490-9bfb-984c9f9f058e",
            "compositeImage": {
                "id": "40ae68c1-aecb-4514-9dd5-23996c63c155",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "048c94da-e99f-430d-bc90-7e8c686e8ccc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9141de7-c8b5-4e9e-8d47-a472cddfb092",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "048c94da-e99f-430d-bc90-7e8c686e8ccc",
                    "LayerId": "64c8cd15-c7bf-4a9f-8a96-6b8b57519060"
                }
            ]
        },
        {
            "id": "68d90117-9ce1-4694-95d6-1f847e25f8c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b169d6cc-104a-4490-9bfb-984c9f9f058e",
            "compositeImage": {
                "id": "1b5be458-a792-4efa-89b5-505450b7e552",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68d90117-9ce1-4694-95d6-1f847e25f8c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23b03b4e-82d6-475d-888e-2eed2166e6d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68d90117-9ce1-4694-95d6-1f847e25f8c6",
                    "LayerId": "64c8cd15-c7bf-4a9f-8a96-6b8b57519060"
                }
            ]
        },
        {
            "id": "e0ff444e-2a5f-4a97-a866-0e0645609455",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b169d6cc-104a-4490-9bfb-984c9f9f058e",
            "compositeImage": {
                "id": "6025abf7-6be7-45d9-b917-6983802a9551",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0ff444e-2a5f-4a97-a866-0e0645609455",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae85bec3-e063-4289-878d-beb18c08b345",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0ff444e-2a5f-4a97-a866-0e0645609455",
                    "LayerId": "64c8cd15-c7bf-4a9f-8a96-6b8b57519060"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "64c8cd15-c7bf-4a9f-8a96-6b8b57519060",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b169d6cc-104a-4490-9bfb-984c9f9f058e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 26,
    "xorig": 0,
    "yorig": 0
}