{
    "id": "321f1426-75b3-4a84-8e2f-30ac0ff1a5f7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_grass",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 1,
    "bbox_right": 22,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1429dc42-075c-4211-96ce-20b44cd9fca8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "321f1426-75b3-4a84-8e2f-30ac0ff1a5f7",
            "compositeImage": {
                "id": "082a120a-349c-4990-8124-2f0821dedd08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1429dc42-075c-4211-96ce-20b44cd9fca8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "064fd9d1-e60b-461a-b0c9-abcd205be1a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1429dc42-075c-4211-96ce-20b44cd9fca8",
                    "LayerId": "247c2602-49d1-4c43-8215-17d8b859dd66"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "247c2602-49d1-4c43-8215-17d8b859dd66",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "321f1426-75b3-4a84-8e2f-30ac0ff1a5f7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}