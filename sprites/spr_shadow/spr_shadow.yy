{
    "id": "4cefdf3c-7dde-41af-becc-fa16b687f9e8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_shadow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cbfc3ede-ae8d-4f7d-99f8-9dcec2f78148",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cefdf3c-7dde-41af-becc-fa16b687f9e8",
            "compositeImage": {
                "id": "1db501c5-d7de-46ef-8038-b77f2710b998",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbfc3ede-ae8d-4f7d-99f8-9dcec2f78148",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29cb728e-fa64-44a7-8798-f2a532b878c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbfc3ede-ae8d-4f7d-99f8-9dcec2f78148",
                    "LayerId": "96a99d64-6be7-4a0f-b3e9-985dc900c747"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "96a99d64-6be7-4a0f-b3e9-985dc900c747",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4cefdf3c-7dde-41af-becc-fa16b687f9e8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}