{
    "id": "86c2c37f-7ab7-4c8d-b86e-70c9285c5150",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite24",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 444,
    "bbox_left": 0,
    "bbox_right": 1610,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "000d1f84-4232-46c3-82b0-25b9d0d5791f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86c2c37f-7ab7-4c8d-b86e-70c9285c5150",
            "compositeImage": {
                "id": "048c7a20-6722-416e-8736-e491c2986f9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "000d1f84-4232-46c3-82b0-25b9d0d5791f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88bcd01f-106d-4b25-b9c2-9393abb5762f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "000d1f84-4232-46c3-82b0-25b9d0d5791f",
                    "LayerId": "f511f29c-6661-43be-be59-daf1c814a0e6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 445,
    "layers": [
        {
            "id": "f511f29c-6661-43be-be59-daf1c814a0e6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "86c2c37f-7ab7-4c8d-b86e-70c9285c5150",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1611,
    "xorig": 0,
    "yorig": 0
}