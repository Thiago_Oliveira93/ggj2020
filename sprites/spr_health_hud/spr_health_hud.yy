{
    "id": "c873372d-ac40-4290-8905-edf1f6661b13",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_health_hud",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 0,
    "bbox_right": 37,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d77f61e2-7e7f-4589-922a-4323227839f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c873372d-ac40-4290-8905-edf1f6661b13",
            "compositeImage": {
                "id": "abc52c4c-4c09-444a-96d8-a5a217ed8a69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d77f61e2-7e7f-4589-922a-4323227839f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b02215cf-1062-4ccf-8741-b6cb5383875e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d77f61e2-7e7f-4589-922a-4323227839f6",
                    "LayerId": "d8d21337-db0f-4180-a4e3-d310b27c8964"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "d8d21337-db0f-4180-a4e3-d310b27c8964",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c873372d-ac40-4290-8905-edf1f6661b13",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 130,
    "xorig": 0,
    "yorig": 0
}