{
    "id": "124b289c-3abc-4f2b-b4ad-f06c4e13a404",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_yellow_enemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 1,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e67b1262-8a03-424f-8cab-70dedfe266d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "124b289c-3abc-4f2b-b4ad-f06c4e13a404",
            "compositeImage": {
                "id": "bcc29a9b-4faf-4f1f-975d-c1c40b06d464",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e67b1262-8a03-424f-8cab-70dedfe266d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba8a897e-4d62-4c33-ab3b-9b3de1b36d21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e67b1262-8a03-424f-8cab-70dedfe266d6",
                    "LayerId": "b4458e0a-576c-47b3-9b15-12e5204ecc60"
                }
            ]
        },
        {
            "id": "dce8ef44-faa1-43a2-b7b7-782b33c34c49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "124b289c-3abc-4f2b-b4ad-f06c4e13a404",
            "compositeImage": {
                "id": "6a1e69d5-79d5-4154-8910-471766f543ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dce8ef44-faa1-43a2-b7b7-782b33c34c49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7808dd0e-4d84-4561-88b0-59ea3e9fb3c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dce8ef44-faa1-43a2-b7b7-782b33c34c49",
                    "LayerId": "b4458e0a-576c-47b3-9b15-12e5204ecc60"
                }
            ]
        },
        {
            "id": "b36e6af2-75b9-4421-8e88-436a85c177fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "124b289c-3abc-4f2b-b4ad-f06c4e13a404",
            "compositeImage": {
                "id": "ef7b8b16-d3e5-45b7-8486-93bbac7aa1f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b36e6af2-75b9-4421-8e88-436a85c177fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c980ea64-e93b-4c01-9c2b-569b65da41ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b36e6af2-75b9-4421-8e88-436a85c177fd",
                    "LayerId": "b4458e0a-576c-47b3-9b15-12e5204ecc60"
                }
            ]
        },
        {
            "id": "b29fc581-bf99-436b-a407-2bf5fd45f448",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "124b289c-3abc-4f2b-b4ad-f06c4e13a404",
            "compositeImage": {
                "id": "afd4319e-b17b-46bc-be01-3275f2855d51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b29fc581-bf99-436b-a407-2bf5fd45f448",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48602bf4-9692-4415-bc97-71a00e7d5ee1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b29fc581-bf99-436b-a407-2bf5fd45f448",
                    "LayerId": "b4458e0a-576c-47b3-9b15-12e5204ecc60"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "b4458e0a-576c-47b3-9b15-12e5204ecc60",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "124b289c-3abc-4f2b-b4ad-f06c4e13a404",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 12
}