{
    "id": "7901d1be-1223-4e87-a4ee-89d906848210",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_attack_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 1,
    "bbox_right": 22,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fc49d31e-8956-4a0d-882d-217f49d475f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7901d1be-1223-4e87-a4ee-89d906848210",
            "compositeImage": {
                "id": "768c5319-bc52-4272-a2ac-681b268fd67f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc49d31e-8956-4a0d-882d-217f49d475f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44617a88-03b4-43d8-976b-77e07130f3ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc49d31e-8956-4a0d-882d-217f49d475f6",
                    "LayerId": "224d4697-f541-435e-ac9c-1c8e8efd194e"
                }
            ]
        },
        {
            "id": "2165b195-75db-4b36-9ab9-2e769cd1815c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7901d1be-1223-4e87-a4ee-89d906848210",
            "compositeImage": {
                "id": "122a3a3b-23ca-419a-9ff6-a7468072ca33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2165b195-75db-4b36-9ab9-2e769cd1815c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1a7aa6a-a96d-4274-8045-e5b3842bb10c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2165b195-75db-4b36-9ab9-2e769cd1815c",
                    "LayerId": "224d4697-f541-435e-ac9c-1c8e8efd194e"
                }
            ]
        },
        {
            "id": "bdc57038-49a9-4696-9bd1-e30e6f920c12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7901d1be-1223-4e87-a4ee-89d906848210",
            "compositeImage": {
                "id": "0baa7f1f-1f7d-414b-9979-7e7368ac801d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdc57038-49a9-4696-9bd1-e30e6f920c12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a473441-d1f6-41ac-8079-9bb3f5940057",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdc57038-49a9-4696-9bd1-e30e6f920c12",
                    "LayerId": "224d4697-f541-435e-ac9c-1c8e8efd194e"
                }
            ]
        },
        {
            "id": "6556485d-8390-4124-b86c-43d276ee8249",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7901d1be-1223-4e87-a4ee-89d906848210",
            "compositeImage": {
                "id": "ce76c9e0-7a1e-462f-8005-fdd872cd99d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6556485d-8390-4124-b86c-43d276ee8249",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d83f1e5b-9450-4cad-b152-848ee4953838",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6556485d-8390-4124-b86c-43d276ee8249",
                    "LayerId": "224d4697-f541-435e-ac9c-1c8e8efd194e"
                }
            ]
        },
        {
            "id": "22ff4782-1158-4211-b804-985f9ea91f80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7901d1be-1223-4e87-a4ee-89d906848210",
            "compositeImage": {
                "id": "87bd4edc-af64-4335-9728-4c71472443a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22ff4782-1158-4211-b804-985f9ea91f80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "621a303d-9617-45b0-aa8c-305419d92e24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22ff4782-1158-4211-b804-985f9ea91f80",
                    "LayerId": "224d4697-f541-435e-ac9c-1c8e8efd194e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "224d4697-f541-435e-ac9c-1c8e8efd194e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7901d1be-1223-4e87-a4ee-89d906848210",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}