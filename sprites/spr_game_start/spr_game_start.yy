{
    "id": "3f725215-5e51-416f-8b52-dcb5b92d1ce4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_game_start",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 359,
    "bbox_left": 0,
    "bbox_right": 459,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a35ff915-c616-4ea4-acb6-dba41b586cad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f725215-5e51-416f-8b52-dcb5b92d1ce4",
            "compositeImage": {
                "id": "95a75041-9eb3-4e6d-96b3-b7e4e8666239",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a35ff915-c616-4ea4-acb6-dba41b586cad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2dd0e744-c194-4a05-91b6-3086028e6ca7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a35ff915-c616-4ea4-acb6-dba41b586cad",
                    "LayerId": "01f5a288-a964-46a4-9d6b-622efb2a097e"
                }
            ]
        },
        {
            "id": "05313fee-6699-4f34-bc75-4409921d2399",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f725215-5e51-416f-8b52-dcb5b92d1ce4",
            "compositeImage": {
                "id": "e9c70af5-ea6e-498d-8af0-ecae64f62936",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05313fee-6699-4f34-bc75-4409921d2399",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b00089e2-6f2f-41c4-9541-29ea544e743e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05313fee-6699-4f34-bc75-4409921d2399",
                    "LayerId": "01f5a288-a964-46a4-9d6b-622efb2a097e"
                }
            ]
        },
        {
            "id": "883bfc0e-e607-4f90-b6b5-dd13f6ecb4cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f725215-5e51-416f-8b52-dcb5b92d1ce4",
            "compositeImage": {
                "id": "5c1aa1cf-515d-443c-9a1e-81ccc526a502",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "883bfc0e-e607-4f90-b6b5-dd13f6ecb4cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "815bbcfd-84b7-486c-8169-b5f4f71807c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "883bfc0e-e607-4f90-b6b5-dd13f6ecb4cb",
                    "LayerId": "01f5a288-a964-46a4-9d6b-622efb2a097e"
                }
            ]
        },
        {
            "id": "b7c77885-2e27-432b-9dd0-a5cdfb21514f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f725215-5e51-416f-8b52-dcb5b92d1ce4",
            "compositeImage": {
                "id": "3db7f6dc-40dc-4eeb-89cd-85bea20bcebf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7c77885-2e27-432b-9dd0-a5cdfb21514f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2efef224-529c-4a34-86f6-f926a7de52f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7c77885-2e27-432b-9dd0-a5cdfb21514f",
                    "LayerId": "01f5a288-a964-46a4-9d6b-622efb2a097e"
                }
            ]
        },
        {
            "id": "dd0d8388-931c-482f-a61c-be0313953ccd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f725215-5e51-416f-8b52-dcb5b92d1ce4",
            "compositeImage": {
                "id": "32e6cc70-7945-4500-8a71-78b5ee94668f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd0d8388-931c-482f-a61c-be0313953ccd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd5d84db-02a6-4ded-b96a-db8fb7322622",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd0d8388-931c-482f-a61c-be0313953ccd",
                    "LayerId": "01f5a288-a964-46a4-9d6b-622efb2a097e"
                }
            ]
        },
        {
            "id": "6e5fa072-4e3b-4115-81cf-692b4a0ffb58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f725215-5e51-416f-8b52-dcb5b92d1ce4",
            "compositeImage": {
                "id": "631b5ac6-a896-4775-bc5c-e6791bf245d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e5fa072-4e3b-4115-81cf-692b4a0ffb58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f3953f0-f18e-45b9-86d8-9f5df7dedad8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e5fa072-4e3b-4115-81cf-692b4a0ffb58",
                    "LayerId": "01f5a288-a964-46a4-9d6b-622efb2a097e"
                }
            ]
        },
        {
            "id": "591e9089-ce49-4cce-8019-34a2252b9675",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f725215-5e51-416f-8b52-dcb5b92d1ce4",
            "compositeImage": {
                "id": "6fec4c66-83ce-4939-b1fe-4485ed25624d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "591e9089-ce49-4cce-8019-34a2252b9675",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d02fd80e-108e-488f-9469-9140776dc4f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "591e9089-ce49-4cce-8019-34a2252b9675",
                    "LayerId": "01f5a288-a964-46a4-9d6b-622efb2a097e"
                }
            ]
        },
        {
            "id": "c052cb06-82eb-4355-a333-18abdcc1745b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f725215-5e51-416f-8b52-dcb5b92d1ce4",
            "compositeImage": {
                "id": "27bad955-eb28-4a1e-9eb7-9bc7f6cb274d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c052cb06-82eb-4355-a333-18abdcc1745b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa976a82-c5d7-4ecd-9560-40c6b3e84428",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c052cb06-82eb-4355-a333-18abdcc1745b",
                    "LayerId": "01f5a288-a964-46a4-9d6b-622efb2a097e"
                }
            ]
        },
        {
            "id": "c101b0a5-8463-4d2c-9af8-b02df445345d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f725215-5e51-416f-8b52-dcb5b92d1ce4",
            "compositeImage": {
                "id": "d5e0db52-82b1-454e-b377-6b55b3ad4241",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c101b0a5-8463-4d2c-9af8-b02df445345d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84c81edf-147a-4672-b94b-47240eee4fa5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c101b0a5-8463-4d2c-9af8-b02df445345d",
                    "LayerId": "01f5a288-a964-46a4-9d6b-622efb2a097e"
                }
            ]
        },
        {
            "id": "bb72b73a-4d81-4434-823b-860b793a4c16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f725215-5e51-416f-8b52-dcb5b92d1ce4",
            "compositeImage": {
                "id": "6faa42e5-f66e-4b60-9262-a6660510aabf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb72b73a-4d81-4434-823b-860b793a4c16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2dfdd04d-b897-403f-b3f6-9bfe886c08a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb72b73a-4d81-4434-823b-860b793a4c16",
                    "LayerId": "01f5a288-a964-46a4-9d6b-622efb2a097e"
                }
            ]
        },
        {
            "id": "7d08d2de-764e-44a7-95d9-ee7e278bdcac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f725215-5e51-416f-8b52-dcb5b92d1ce4",
            "compositeImage": {
                "id": "91ae5405-2bb6-4d0d-b037-be0e63e7fa07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d08d2de-764e-44a7-95d9-ee7e278bdcac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4463c459-cfb0-4ea4-b4ec-f84e529f7595",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d08d2de-764e-44a7-95d9-ee7e278bdcac",
                    "LayerId": "01f5a288-a964-46a4-9d6b-622efb2a097e"
                }
            ]
        },
        {
            "id": "8d324e49-38a5-4c91-9810-198385ba8205",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f725215-5e51-416f-8b52-dcb5b92d1ce4",
            "compositeImage": {
                "id": "7c8223a0-a860-4228-824a-377940db657c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d324e49-38a5-4c91-9810-198385ba8205",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0ac2214-a849-4ccd-b38c-0c8f15e014da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d324e49-38a5-4c91-9810-198385ba8205",
                    "LayerId": "01f5a288-a964-46a4-9d6b-622efb2a097e"
                }
            ]
        },
        {
            "id": "e97305d3-c50d-4bdf-89ef-605aba910cd3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f725215-5e51-416f-8b52-dcb5b92d1ce4",
            "compositeImage": {
                "id": "0488a9cb-9407-42d3-a50b-1e6927b22323",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e97305d3-c50d-4bdf-89ef-605aba910cd3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c9db591-5d14-463d-8dd3-1420f1ab9337",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e97305d3-c50d-4bdf-89ef-605aba910cd3",
                    "LayerId": "01f5a288-a964-46a4-9d6b-622efb2a097e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "01f5a288-a964-46a4-9d6b-622efb2a097e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3f725215-5e51-416f-8b52-dcb5b92d1ce4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 460,
    "xorig": 230,
    "yorig": 180
}