{
    "id": "dad061a2-d6dc-4703-ae81-0975c33aaf11",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_red_enemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 1,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4f2da5cb-d571-4ad0-8959-84dc99f7f58c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dad061a2-d6dc-4703-ae81-0975c33aaf11",
            "compositeImage": {
                "id": "e35c5059-9f60-4003-ac65-961916f888e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f2da5cb-d571-4ad0-8959-84dc99f7f58c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5988e05e-61ff-4b73-a33c-1fd61dcbcc8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f2da5cb-d571-4ad0-8959-84dc99f7f58c",
                    "LayerId": "307319ad-fb2c-428c-bc7a-a35be905b2e1"
                }
            ]
        },
        {
            "id": "a0f90497-82b1-47cc-af9d-ba74e83033d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dad061a2-d6dc-4703-ae81-0975c33aaf11",
            "compositeImage": {
                "id": "ac6970ec-a338-42a7-9200-8287e307261a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0f90497-82b1-47cc-af9d-ba74e83033d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98fd1966-593e-40b3-9b8d-45e51844ee98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0f90497-82b1-47cc-af9d-ba74e83033d2",
                    "LayerId": "307319ad-fb2c-428c-bc7a-a35be905b2e1"
                }
            ]
        },
        {
            "id": "5f1ba879-f4d9-4d73-96c7-2b5de6d11d60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dad061a2-d6dc-4703-ae81-0975c33aaf11",
            "compositeImage": {
                "id": "18f6d73b-4ccf-4ad1-a26c-696a60f086a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f1ba879-f4d9-4d73-96c7-2b5de6d11d60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6f1873d-007e-45da-8dc0-3b459d5d2f1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f1ba879-f4d9-4d73-96c7-2b5de6d11d60",
                    "LayerId": "307319ad-fb2c-428c-bc7a-a35be905b2e1"
                }
            ]
        },
        {
            "id": "97c84734-06da-4675-b3bb-36f20765b9d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dad061a2-d6dc-4703-ae81-0975c33aaf11",
            "compositeImage": {
                "id": "896dab43-35df-4bbb-a2ab-de8d441fcdac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97c84734-06da-4675-b3bb-36f20765b9d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a48ee83-774e-4dce-bf73-4db734e5435d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97c84734-06da-4675-b3bb-36f20765b9d2",
                    "LayerId": "307319ad-fb2c-428c-bc7a-a35be905b2e1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "307319ad-fb2c-428c-bc7a-a35be905b2e1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dad061a2-d6dc-4703-ae81-0975c33aaf11",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 12
}