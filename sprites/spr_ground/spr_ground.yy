{
    "id": "51013c9e-a55e-4ed4-83ad-ba3a7144e8d4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 16,
    "bbox_right": 111,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6c881c00-c3fe-45be-9d5e-dc6ba046f062",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51013c9e-a55e-4ed4-83ad-ba3a7144e8d4",
            "compositeImage": {
                "id": "b73c2542-5073-4440-827d-6b1241999f17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c881c00-c3fe-45be-9d5e-dc6ba046f062",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e648f85-6f32-4319-9783-206d3558fcc4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c881c00-c3fe-45be-9d5e-dc6ba046f062",
                    "LayerId": "6dda2a7a-48fe-405b-a820-3e5bfb58526c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "6dda2a7a-48fe-405b-a820-3e5bfb58526c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "51013c9e-a55e-4ed4-83ad-ba3a7144e8d4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 112,
    "xorig": 0,
    "yorig": 0
}