{
    "id": "0896fc0f-7043-403f-816a-86c73d1b8ab2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "46b7c302-876f-47e3-8171-a2edbaed34da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0896fc0f-7043-403f-816a-86c73d1b8ab2",
            "compositeImage": {
                "id": "b5cda9fb-03c9-4a19-8040-326b285ec056",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46b7c302-876f-47e3-8171-a2edbaed34da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43fc67bf-4318-4e86-997a-2fd73bb75d7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46b7c302-876f-47e3-8171-a2edbaed34da",
                    "LayerId": "3ea64df2-3cea-4a7c-8152-c29eb2cfc2eb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "3ea64df2-3cea-4a7c-8152-c29eb2cfc2eb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0896fc0f-7043-403f-816a-86c73d1b8ab2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 12
}