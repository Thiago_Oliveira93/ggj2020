{
    "id": "4ab75a60-de38-4683-9bed-7e40fb2def0e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_altar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 1,
    "bbox_right": 22,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2ff58595-ebea-42f6-85fb-3479a42e0a9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ab75a60-de38-4683-9bed-7e40fb2def0e",
            "compositeImage": {
                "id": "12754ada-14f9-40fd-a4ef-6a2863b25f9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ff58595-ebea-42f6-85fb-3479a42e0a9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4dd2c127-f6b0-448f-b348-e05daaba04c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ff58595-ebea-42f6-85fb-3479a42e0a9e",
                    "LayerId": "71890249-9b34-4208-b875-5743588fd7e7"
                }
            ]
        },
        {
            "id": "918718a8-6ebf-48aa-96b7-d25372a23b2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ab75a60-de38-4683-9bed-7e40fb2def0e",
            "compositeImage": {
                "id": "49bf6d8f-4334-4bfb-b9b1-ed846de9df8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "918718a8-6ebf-48aa-96b7-d25372a23b2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e639bb9-86fc-4991-9789-af3b48c66b29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "918718a8-6ebf-48aa-96b7-d25372a23b2f",
                    "LayerId": "71890249-9b34-4208-b875-5743588fd7e7"
                }
            ]
        },
        {
            "id": "31c9b41e-b1bf-47d3-94be-a40e50965ee8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ab75a60-de38-4683-9bed-7e40fb2def0e",
            "compositeImage": {
                "id": "2da722d8-733f-49c1-9e0e-919a227c36fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31c9b41e-b1bf-47d3-94be-a40e50965ee8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b76f655f-1263-4474-b063-78cdd5652b9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31c9b41e-b1bf-47d3-94be-a40e50965ee8",
                    "LayerId": "71890249-9b34-4208-b875-5743588fd7e7"
                }
            ]
        },
        {
            "id": "a425b1ec-a19f-4494-bf51-f87a76b792ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ab75a60-de38-4683-9bed-7e40fb2def0e",
            "compositeImage": {
                "id": "8f9ebcad-8715-413e-a476-c22079e5ca80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a425b1ec-a19f-4494-bf51-f87a76b792ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd9497c9-8d5c-418b-95a6-b3e015c1a16e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a425b1ec-a19f-4494-bf51-f87a76b792ff",
                    "LayerId": "71890249-9b34-4208-b875-5743588fd7e7"
                }
            ]
        },
        {
            "id": "4a475e87-b4e7-4951-972c-1e1841348523",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ab75a60-de38-4683-9bed-7e40fb2def0e",
            "compositeImage": {
                "id": "2d4a6c56-780e-4a45-a1bf-dcb873f8773b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a475e87-b4e7-4951-972c-1e1841348523",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc4c6867-7bbc-4d71-b186-9cb62a64ca07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a475e87-b4e7-4951-972c-1e1841348523",
                    "LayerId": "71890249-9b34-4208-b875-5743588fd7e7"
                }
            ]
        },
        {
            "id": "f7fabc1e-2f16-43fd-a1bf-2df2d976c296",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ab75a60-de38-4683-9bed-7e40fb2def0e",
            "compositeImage": {
                "id": "36bf83ab-a34f-4857-b96a-613317d0687b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7fabc1e-2f16-43fd-a1bf-2df2d976c296",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7adb093f-1b22-4174-ac3b-d9e5d6059679",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7fabc1e-2f16-43fd-a1bf-2df2d976c296",
                    "LayerId": "71890249-9b34-4208-b875-5743588fd7e7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "71890249-9b34-4208-b875-5743588fd7e7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4ab75a60-de38-4683-9bed-7e40fb2def0e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}